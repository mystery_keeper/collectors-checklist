#ifndef DURATIONSPINBOX_H_INCLUDED
#define DURATIONSPINBOX_H_INCLUDED

#include <QAbstractSpinBox>
#include <QSet>

#include "ccduration.h"

/// Spinbox widget for duration type model item.
/// Intended for use with qulonglong/uint64_t milliseconds number. Represents
/// duration if format D:HH:MM:SS.MS. When displayed, zero parts are omited,
/// when possible. When editting, full format is used.
class DurationSpinBox final : public QAbstractSpinBox
{
	Q_OBJECT

	public:

		/// Constructor.
		DurationSpinBox(QWidget * parent = 0);

		/// Destructor.
		~DurationSpinBox();

		/// \return The total number of milliseconds in duration.
		qulonglong value() const;

		/// Sets duration from total number of milliseconds.
		void setValue(qulonglong newValue);

		/// Validates the attempted input.
		/// Reimplemented from QAbstractSpinbox.
		QValidator::State validate(QString & input, int & pos) const override;

		/// Tries to change invalid input into valid.
		/// Reimplemented from QAbstractSpinbox.
		void fixup(QString & input) const override;

		/// Modifies the internal value.
		/// Called by internal functions.
		/// Reimplemented from QAbstractSpinbox.
		void stepBy(int steps) override;

	protected:

		/// Checks if stepping up and down is possible.
		/// Called by internal functions.
		/// Reimplemented from QAbstractSpinbox.
		QAbstractSpinBox::StepEnabled stepEnabled() const override;

	private:

		/// Set of the characters, acceptable for duration input.
		/// Used in input validation.
		QSet<QChar> m_acceptableCharactersSet;

		/// Creates a duration object from the input, assuming the input valid.
		CC::Duration textToDuration() const;
};

#endif // DURATIONSPINBOX_H_INCLUDED
