#include <QComboBox>

#include "ccfield.h"
#include "itemdelegateforfieldtype.h"

//==============================================================================

CC::ItemDelegateForFieldType::ItemDelegateForFieldType(QObject * parent):
	QStyledItemDelegate(parent)
{

}

//	END OF CC::ItemDelegateForFieldType::ItemDelegateForFieldType(
//		QObject * parent)
//==============================================================================

CC::ItemDelegateForFieldType::~ItemDelegateForFieldType()
{

}

//	END OF CC::ItemDelegateForFieldType::~ItemDelegateForFieldType()
//==============================================================================

QWidget * CC::ItemDelegateForFieldType::createEditor(QWidget * parent,
	const QStyleOptionViewItem & /*option*/, const QModelIndex & /*index*/)
	const
{
	QComboBox * fieldTypesList = new QComboBox(parent);

	fieldTypesList->addItem(QObject::trUtf8("Text"),
		QVariant(CC::FieldType::Text));
	fieldTypesList->addItem(QObject::trUtf8("Integer"),
		QVariant(CC::FieldType::Integer));
	fieldTypesList->addItem(QObject::trUtf8("Real"),
		QVariant(CC::FieldType::Real));
	fieldTypesList->addItem(QObject::trUtf8("Logical"),
		QVariant(CC::FieldType::Logical));
	fieldTypesList->addItem(QObject::trUtf8("Date"),
		QVariant(CC::FieldType::Date));
	fieldTypesList->addItem(QObject::trUtf8("Time"),
		QVariant(CC::FieldType::Time));

	return fieldTypesList;
}

//	END OF QWidget * CC::ItemDelegateForFieldType::createEditor(
//		QWidget * parent, const QStyleOptionViewItem & option,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForFieldType::setEditorData(QWidget * editor,
	const QModelIndex & index) const
{
	QComboBox * fieldTypesList = (QComboBox *)editor;

	QVariant fieldType = index.model()->data(index, Qt::EditRole);

	for(int i = 0; i < fieldTypesList->count(); ++i)
	{
		if(fieldType == fieldTypesList->itemData(i))
		{
			fieldTypesList->setCurrentIndex(i);
			return;
		}
	}

	return;
}

//	END OF CC::ItemDelegateForFieldType::setEditorData(QWidget * editor,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForFieldType::updateEditorGeometry(QWidget * editor,
	const QStyleOptionViewItem & option, const QModelIndex & /*index*/)
	const
{
	editor->setGeometry(option.rect);
	return;
}

//	END OF void CC::ItemDelegateForFieldType::updateEditorGeometry(
//		QWidget * editor, const QStyleOptionViewItem & option,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForFieldType::setModelData(QWidget * editor,
	QAbstractItemModel * model, const QModelIndex & index) const
{
	QComboBox * fieldTypesList = (QComboBox *)editor;
	QVariant value = fieldTypesList->itemData(fieldTypesList->currentIndex());
	model->setData(index, value, Qt::EditRole);
	return;
}

//	END OF void CC::ItemDelegateForFieldType::setModelData(QWidget * editor,
//		QAbstractItemModel * model, const QModelIndex & index) const
//==============================================================================
