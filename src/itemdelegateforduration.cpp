#include "durationspinbox.h"
#include "itemdelegateforduration.h"

//==============================================================================

CC::ItemDelegateForDuration::ItemDelegateForDuration(QObject * parent):
	QStyledItemDelegate(parent)
{

}

//	END OF CC::ItemDelegateForDuration::ItemDelegateForDuration(
//		QObject * parent)
//==============================================================================

CC::ItemDelegateForDuration::~ItemDelegateForDuration()
{

}

//	END OF CC::ItemDelegateForDuration::~ItemDelegateForDuration()
//==============================================================================

QWidget * CC::ItemDelegateForDuration::createEditor(QWidget * parent,
	const QStyleOptionViewItem & /*option*/, const QModelIndex & /*index*/)
	const
{
	DurationSpinBox * valueSpinBox = new DurationSpinBox(parent);

	return valueSpinBox;
}

//	END OF QWidget * CC::ItemDelegateForDuration::createEditor(
//		QWidget * parent, const QStyleOptionViewItem & option,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForDuration::setEditorData(QWidget * editor,
	const QModelIndex & index) const
{
	DurationSpinBox * valueSpinBox = (DurationSpinBox *)editor;

	QVariant newValue = index.model()->data(index, Qt::EditRole);

	valueSpinBox->setValue(newValue.toULongLong());

	return;
}

//	END OF CC::ItemDelegateForDuration::setEditorData(QWidget * editor,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForDuration::updateEditorGeometry(QWidget * editor,
	const QStyleOptionViewItem & option, const QModelIndex & /*index*/)
	const
{
	editor->setGeometry(option.rect);
	return;
}

//	END OF void CC::ItemDelegateForDuration::updateEditorGeometry(
//		QWidget * editor, const QStyleOptionViewItem & option,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForDuration::setModelData(QWidget * editor,
	QAbstractItemModel * model, const QModelIndex & index) const
{
	DurationSpinBox * valueSpinBox = (DurationSpinBox *)editor;
	QVariant newValue(valueSpinBox->value());
	model->setData(index, newValue, Qt::EditRole);
	return;
}

//	END OF void CC::ItemDelegateForDuration::setModelData(QWidget * editor,
//		QAbstractItemModel * model, const QModelIndex & index) const
//==============================================================================
