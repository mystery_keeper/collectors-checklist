#ifndef DURATION_H_INCLUDED
#define DURATION_H_INCLUDED

#include <stdint.h>
#include <string>

namespace CC
{
    /// Class for handling duration type. Maximum duration equals to unsigned
    /// 64 bit integer maximum of milliseconds as it converts to and from
    /// uint64_t milliseconds number. Stores duration as separate numbers of
    /// days, hours, minutes, seconds and milliseconds. Uses overflow checks
    /// at creation/set time and provides the static method that validates
    /// if separate numbers are convertible to uint64_t number of milliseconds.
    /// Provides full and compact string representation. Full representation is
    /// in format D:HH:MM:SS.MS. Compact reperesentation omits zero parts when
    /// possible, but minutes and seconds are always included.

	class Duration final
	{
		public:

			/// Default constructor. Duration defaults to 0 milliseconds.
			Duration();

			/// Constructor. Parses milliseconds to component parts.
			Duration(uint64_t totalMilliseconds);

			/// Constructor. Validates if input is presentable in uint64_t
			/// milliseconds. Defaults all parts to 0 if validation fails.
			Duration(uint64_t days, uint8_t hours, uint8_t minutes,
				uint8_t seconds, uint16_t milliseconds);

			/// Copy constructor.
			Duration(const CC::Duration& other);

			/// Copy operator.
			Duration& operator=(const CC::Duration& other);

			/// Destructor.
			~Duration();

			/// Days getter.
			uint64_t days() const;
			/// Hours getter.
			uint8_t hours() const;
			/// Minutes getter.
			uint8_t minutes() const;
			/// Seconds getter.
			uint8_t seconds() const;
			/// Milliseconds getter.
			uint16_t milliseconds() const;
			/// \return total number of milliseconds, calculated from stored
			/// 	parts.
			uint64_t totalMilliseconds() const;

			/// Sets duration.
			/// Parses milliseconds to days and so on.
			/// \return Always true.
			bool set(uint64_t totalMilliseconds);

			/// Sets duration.
			/// Validates if input is presentable in uint64_t milliseconds.
			/// If validation fails, no internal values are changed.
			/// \return True, if input passes validation. False otherwise.
			bool set(uint64_t days, uint8_t hours, uint8_t minutes,
				uint8_t seconds, uint16_t milliseconds);

			/// \return Full string representation.
			std::wstring toWStringFull() const;

			/// \return Compact string representation.
			std::wstring toWStringCompact() const;

			/// Validates if input is presentable in uint64_t milliseconds.
			/// \return True, if input passes validation. False otherwise.
			static bool validate(uint64_t days, uint8_t hours, uint8_t minutes,
				uint8_t seconds, uint16_t milliseconds);

			/// Parses duration in milliseconds into component parts.
			static void parseMilliseconds(uint64_t totalMilliseconds,
				uint64_t & days, uint8_t & hours, uint8_t & minutes,
				uint8_t & seconds, uint16_t & milliseconds);

			/// Maximum number of days that may be presented as uint64_t
			/// milliseconds. It stil may not be presented as such with
			/// addition of certain number of hours, minutes, seconds and
			/// milliseconds.
			static const uint64_t MAX_DAYS = 213503982334ull;

			/// Constant, used in calculations.
			static const uint64_t MILLISECONDS_IN_SECOND = 1000ull;
			/// Constant, used in calculations.
			static const uint64_t MILLISECONDS_IN_MINUTE = 60000ull;
			/// Constant, used in calculations.
			static const uint64_t MILLISECONDS_IN_HOUR = 3600000ull;
			/// Constant, used in calculations.
			static const uint64_t MILLISECONDS_IN_DAY = 86400000ull;

		private:

			/// Stored number of days.
			uint64_t m_days;
			/// Stored number of hours.
			uint8_t m_hours;
			/// Stored number of minutes.
			uint8_t m_minutes;
			/// Stored number of seconds.
			uint8_t m_seconds;
			/// Stored number of milliseconds.
			uint16_t m_milliseconds;
	};
}

#endif // DURATION_H_INCLUDED
