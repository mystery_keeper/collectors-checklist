#ifndef LONGLONGSPINBOX_H_INCLUDED
#define LONGLONGSPINBOX_H_INCLUDED

#include <QAbstractSpinBox>
#include <QRegExpValidator>

/// Spinbox widget for 64 bits integer value.
class LongLongSpinBox final : public QAbstractSpinBox
{
	Q_OBJECT

	public:

		/// Constructor.
		LongLongSpinBox(qlonglong newValue = 0ull, QWidget * parent = 0);

		/// Destructor.
		~LongLongSpinBox();

		/// \return 64 bit integer in the editor.
		qlonglong value() const;

		/// Sets editor text from the value.
		void setValue(qlonglong newValue);

		/// Validates the attempted input.
		/// Reimplemented from QAbstractSpinbox.
		QValidator::State validate(QString & input, int & pos) const;

		/// Tries to change invalid input into valid.
		/// Reimplemented from QAbstractSpinbox.
		void fixup(QString & input) const;

		/// Modifies the internal value.
		/// Called by internal functions.
		/// Reimplemented from QAbstractSpinbox.
		void stepBy(int steps);

	protected:

		/// Checks if stepping up and down is possible.
		/// Called by internal functions.
		/// Reimplemented from QAbstractSpinbox.
		QAbstractSpinBox::StepEnabled stepEnabled() const;

	private:

		/// RegExp validator for signed integer value.
		QRegExpValidator * m_pSignedIntValidator;
};

#endif // LONGLONGSPINBOX_H_INCLUDED
