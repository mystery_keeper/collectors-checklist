#ifndef FIELDSLISTEDITDIALOG_H_INCLUDED
#define FIELDSLISTEDITDIALOG_H_INCLUDED

#include <ui_fieldslisteditdialog.h>

#include "itemdelegateforfieldtype.h"

#include "fieldslisteditmodel.h"

namespace CC
{
	/// Dialog for editing checklist fields list.
	class FieldsListEditDialog final : public QDialog
	{
		Q_OBJECT

		public:

			/// Constructor.
			FieldsListEditDialog(QWidget * parent = 0, Qt::WindowFlags f = 0);

			/// Destructor.
			virtual ~FieldsListEditDialog();

			/// Opens the dialog with initial fields list to edit.
			int callToEditFieldsList(const QList<CC::Field> & fieldsList);

			/// Gets the resulting fields list after dialog is closed.
			const QList<CC::Field> & fieldsList() const;

		protected:

			/// Dialog close event handler.
			/// Reimplemented from QDialog.
			virtual void closeEvent(QCloseEvent * event) override;

		private:

			/// Qt Designer generated GUI.
			Ui::FieldsListEditDialog ui;

			/// Model that handles the fields list.
			CC::FieldsListEditModel * m_pFieldsListEditModel;

			/// Delegate widget for choosing field type in view widget.
			CC::ItemDelegateForFieldType * m_pItemDelegateForFieldType;

		private slots:

			/// Adds new field to list.
			void slotAddField();

			/// Deletes selected field.
			void slotDeleteSelectedField();

			/// Moves selected field up in the list. Corresponds to moving
			/// the field left in checklist table.
			void slotMoveSelectedFieldUp();

			/// Moves selected field down in the list. Corresponds to moving
			/// the field right in checklist table.
			void slotMoveSelectedFieldDown();

			/// Handles click on OK button.
			void slotOkButtonClicked();
	};
}

#endif // FIELDSLISTEDITDIALOG_H_INCLUDED
