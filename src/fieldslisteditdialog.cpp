#include <QCloseEvent>
#include <QMessageBox>

#include "fieldslisteditdialog.h"

//==============================================================================

CC::FieldsListEditDialog::FieldsListEditDialog(QWidget * parent,
	Qt::WindowFlags windowFlags):
	QDialog(parent, windowFlags)
	, m_pFieldsListEditModel(nullptr)
	, m_pItemDelegateForFieldType(nullptr)
{
	ui.setupUi(this);

	setWindowIcon(QIcon(QString(":setfields.png")));
	ui.btnOK->setIcon(QIcon(QString(":accept.png")));
	ui.btnCancel->setIcon(QIcon(QString(":cancel.png")));
	ui.btnAddField->setIcon(QIcon(QString(":addfield.png")));
	ui.btnDeleteSelectedField->setIcon(QIcon(QString(":deletefield.png")));
	ui.btnMoveSelectedFieldUp->setIcon(QIcon(QString(":movefieldup.png")));
	ui.btnMoveSelectedFieldDown->setIcon(QIcon(QString(":movefielddown.png")));

	connect(ui.btnOK, SIGNAL(clicked()),
		this, SLOT(slotOkButtonClicked()));
	connect(ui.btnCancel, SIGNAL(clicked()),
		this, SLOT(reject()));
	connect(ui.btnAddField, SIGNAL(clicked()),
		this, SLOT(slotAddField()));
	connect(ui.btnDeleteSelectedField, SIGNAL(clicked()),
		this, SLOT(slotDeleteSelectedField()));
	connect(ui.btnMoveSelectedFieldUp, SIGNAL(clicked()),
		this, SLOT(slotMoveSelectedFieldUp()));
	connect(ui.btnMoveSelectedFieldDown, SIGNAL(clicked()),
		this, SLOT(slotMoveSelectedFieldDown()));

	m_pFieldsListEditModel = new CC::FieldsListEditModel(this);

	ui.fieldsListEditView->setModel(m_pFieldsListEditModel);

	m_pItemDelegateForFieldType =
		new CC::ItemDelegateForFieldType(ui.fieldsListEditView);

	ui.fieldsListEditView->setItemDelegateForColumn(1,
		m_pItemDelegateForFieldType);
}

//	END OF CC::FieldsListEditDialog::FieldsListEditDialog(QWidget * parent,
//		Qt::WindowFlags windowFlags)
//==============================================================================

CC::FieldsListEditDialog::~FieldsListEditDialog()
{

}

//	END OF CC::FieldsListEditDialog::~FieldsListEditDialog()
//==============================================================================

int CC::FieldsListEditDialog::callToEditFieldsList(
	const QList<CC::Field> & fieldsList)
{
	m_pFieldsListEditModel->setFieldsList(fieldsList);
	return exec();
}

//	END OF int CC::FieldsListEditDialog::callToEditFieldsList(
//		const QList<CC::Field> & fieldsList)
//==============================================================================

const QList<CC::Field> & CC::FieldsListEditDialog::fieldsList() const
{
	return m_pFieldsListEditModel->fieldsList();
}

//	END OF const QList<CC::Field> & CC::FieldsListEditDialog::fieldsList() const
//==============================================================================

void CC::FieldsListEditDialog::closeEvent(QCloseEvent * event)
{
	event->ignore();
}

//	END OF void CC::FieldsListEditDialog::closeEvent(QCloseEvent * event)
//==============================================================================

void CC::FieldsListEditDialog::slotAddField()
{
	m_pFieldsListEditModel->addField();
}

//	END OF void CC::FieldsListEditDialog::slotAddField()
//==============================================================================

void CC::FieldsListEditDialog::slotDeleteSelectedField()
{
	QModelIndexList selectedItemsList =
		ui.fieldsListEditView->selectionModel()->selectedRows();

	if(selectedItemsList.empty())
	{
		return;
	}

	QMessageBox::StandardButton answer = QMessageBox::question(this,
		QObject::trUtf8("Field deletion"),
		QObject::trUtf8("All data, associated with the deleted field, "
			"will be lost.\n Are you sure you want to delete this field?"),
		QMessageBox::Yes | QMessageBox::No);

	if(answer == QMessageBox::No)
	{
		return;
	}

	m_pFieldsListEditModel->deleteField(selectedItemsList[0].row());

	return;
}

//	END OF void CC::FieldsListEditDialog::slotDeleteSelectedField()
//==============================================================================

void CC::FieldsListEditDialog::slotMoveSelectedFieldUp()
{
	int selectedFieldNumber =
		ui.fieldsListEditView->selectionModel()->selectedRows()[0].row();

	bool result = m_pFieldsListEditModel->moveFieldUp(selectedFieldNumber);

	if(result)
	{
		ui.fieldsListEditView->selectRow(selectedFieldNumber - 1);
	}

	return;
}

//	END OF void CC::FieldsListEditDialog::slotMoveSelectedFieldUp()
//==============================================================================

void CC::FieldsListEditDialog::slotMoveSelectedFieldDown()
{
	int selectedFieldNumber =
		ui.fieldsListEditView->selectionModel()->selectedRows()[0].row();

	bool result = m_pFieldsListEditModel->moveFieldDown(selectedFieldNumber);

	if(result)
	{
		ui.fieldsListEditView->selectRow(selectedFieldNumber + 1);
	}

	return;
}

//	END OF void CC::FieldsListEditDialog::slotMoveSelectedFieldDown()
//==============================================================================

void CC::FieldsListEditDialog::slotOkButtonClicked()
{
	if(!fieldsList().empty())
	{
		accept();
	}
}

//	END OF void CC::FieldsListEditDialog::slotOkButtonClicked()
//==============================================================================
