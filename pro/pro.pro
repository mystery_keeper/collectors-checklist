CONFIG += qt

QT += widgets

CONFIG(debug, debug|release) {

	contains(QMAKE_HOST.arch, x86_64) {
		contains(QMAKE_COMPILER, gcc) {
			DESTDIR = ../build/debug-64bit-gcc
			TARGET = cclist-debug-64bit-gcc
			OBJECTS_DIR = ../generated/obj-debug-gcc
		}
		contains(QMAKE_COMPILER, msvc) {
			DESTDIR = ../build/debug-64bit-msvc
			TARGET = cclist-debug-64bit-msvc
			OBJECTS_DIR = ../generated/obj-debug-64bit-msvc
		}
	} else {
		contains(QMAKE_COMPILER, gcc) {
			DESTDIR = ../build/debug-32bit-gcc
			TARGET = cclist-debug-32bit-gcc
			OBJECTS_DIR = ../generated/obj-debug-32bit-gcc
		}
		contains(QMAKE_COMPILER, msvc) {
			DESTDIR = ../build/debug-32bit-msvc
			TARGET = cclist-debug-32bit-msvc
			OBJECTS_DIR = ../generated/obj-debug-32bit-msvc
		}
	}

	contains(QMAKE_COMPILER, gcc) {
		QMAKE_CXXFLAGS += -O0
		QMAKE_CXXFLAGS += -g
		QMAKE_CXXFLAGS += -ggdb3
		QMAKE_CXXFLAGS += -Wall
		QMAKE_CXXFLAGS += -Wextra
		QMAKE_CXXFLAGS += -Wredundant-decls
		QMAKE_CXXFLAGS += -Wshadow
		#QMAKE_CXXFLAGS += -Weffc++
		QMAKE_CXXFLAGS += -pedantic
	}

} else {

	CONFIG += warn_off

	contains(QMAKE_HOST.arch, x86_64) {
		contains(QMAKE_COMPILER, gcc) {
			DESTDIR = ../build/bin-64bit-gcc
			TARGET = cclist
			OBJECTS_DIR = ../generated/obj-64bit-gcc
		}
		contains(QMAKE_COMPILER, msvc) {
			DESTDIR = ../build/bin-64bit-msvc
			TARGET = cclist
			OBJECTS_DIR = ../generated/obj-64bit-msvc
		}
	} else {
		contains(QMAKE_COMPILER, gcc) {
			DESTDIR = ../build/bin-32bit-gcc
			TARGET = cclist-32bit
			OBJECTS_DIR = ../generated/obj-32bit-gcc
		}
		contains(QMAKE_COMPILER, msvc) {
			DESTDIR = ../build/bin-32bit-msvc
			TARGET = cclist-32bit
			OBJECTS_DIR = ../generated/obj-32bit-msvc
		}
	}

	DEFINES += NDEBUG

	contains(QMAKE_COMPILER, gcc) {
		QMAKE_CXXFLAGS += -O2
		QMAKE_CXXFLAGS += -fexpensive-optimizations
		QMAKE_CXXFLAGS += -funit-at-a-time
	}
}

win32 {
	contains(QMAKE_HOST.arch, x86_64) {
		contains(QMAKE_COMPILER, msvc) {
			QMAKE_LIBDIR += "E:/SDK/msvc-2013-express/VC/lib/amd64/"
			QMAKE_LIBDIR += "C:/Program Files (x86)/Windows Kits/8.1/Lib/winv6.3/um/x64/"
		}
	} else {
		contains(QMAKE_COMPILER, gcc) {
			QMAKE_LFLAGS += -Wl,--large-address-aware
		}
		contains(QMAKE_COMPILER, msvc) {
			QMAKE_LIBDIR += "E:/SDK/msvc-2013-express/VC/lib/"
			QMAKE_LIBDIR += "C:/Program Files (x86)/Windows Kits/8.1/Lib/winv6.3/um/x86/"
			QMAKE_LFLAGS += /LARGEADDRESSAWARE
		}
	}
}

contains(QMAKE_COMPILER, gcc) {
	QMAKE_CXXFLAGS += -std=c++11
	LIBS += -L$$[QT_INSTALL_LIBS]
} else {
	CONFIG += c++11
}

S = $${DIR_SEPARATOR}
D = $$DESTDIR
D = $$replace(D, $$escape_expand(\\), $$S)
D = $$replace(D, /, $$S)
E = $$escape_expand(\n\t)

QMAKE_POST_LINK += $${QMAKE_COPY} ..$${S}README $$D$$S $$E
QMAKE_POST_LINK += $${QMAKE_COPY} ..$${S}LICENSE $$D$$S $$E

TEMPLATE = app

#SUBDIRS

MOC_DIR = ../generated/moc
UI_DIR = ../generated/ui
RCC_DIR = ../generated/rcc

#DEFINES

#TRANSLATIONS

RESOURCES = ../resources/resources.qrc

FORMS += ../src/mainwindow.ui
FORMS += ../src/fieldslisteditdialog.ui

HEADERS += ../src/pugixml/pugiconfig.hpp
HEADERS += ../src/pugixml/pugixml.hpp
HEADERS += ../src/ccfield.h
HEADERS += ../src/ccduration.h
HEADERS += ../src/fieldslisteditmodel.h
HEADERS += ../src/longlongspinbox.h
HEADERS += ../src/durationspinbox.h
HEADERS += ../src/itemdelegatefortext.h
HEADERS += ../src/itemdelegateforinteger.h
HEADERS += ../src/itemdelegateforreal.h
HEADERS += ../src/itemdelegateforlogical.h
HEADERS += ../src/itemdelegatefordate.h
HEADERS += ../src/itemdelegateforduration.h
HEADERS += ../src/itemdelegateforfieldtype.h
HEADERS += ../src/fieldslisteditdialog.h
HEADERS += ../src/checklistmodel.h
HEADERS += ../src/checklistfile.h
HEADERS += ../src/mainwindow.h

SOURCES += ../src/pugixml/pugixml.cpp
SOURCES += ../src/ccduration.cpp
SOURCES += ../src/fieldslisteditmodel.cpp
SOURCES += ../src/longlongspinbox.cpp
SOURCES += ../src/durationspinbox.cpp
SOURCES += ../src/itemdelegatefortext.cpp
SOURCES += ../src/itemdelegateforinteger.cpp
SOURCES += ../src/itemdelegateforreal.cpp
SOURCES += ../src/itemdelegateforlogical.cpp
SOURCES += ../src/itemdelegatefordate.cpp
SOURCES += ../src/itemdelegateforduration.cpp
SOURCES += ../src/itemdelegateforfieldtype.cpp
SOURCES += ../src/fieldslisteditdialog.cpp
SOURCES += ../src/checklistmodel.cpp
SOURCES += ../src/checklistfile.cpp
SOURCES += ../src/mainwindow.cpp
SOURCES += ../src/main.cpp
