#include "longlongspinbox.h"
#include "itemdelegateforinteger.h"

//==============================================================================

CC::ItemDelegateForInteger::ItemDelegateForInteger(QObject * parent):
	QStyledItemDelegate(parent)
{

}

//	END OF CC::ItemDelegateForInteger::ItemDelegateForInteger(
//		QObject * parent)
//==============================================================================

CC::ItemDelegateForInteger::~ItemDelegateForInteger()
{

}

//	END OF CC::ItemDelegateForInteger::~ItemDelegateForInteger()
//==============================================================================

QWidget * CC::ItemDelegateForInteger::createEditor(QWidget * parent,
	const QStyleOptionViewItem & /*option*/, const QModelIndex & /*index*/)
	const
{
	LongLongSpinBox * valueSpinBox = new LongLongSpinBox(0ull, parent);

	return valueSpinBox;
}

//	END OF QWidget * CC::ItemDelegateForInteger::createEditor(
//		QWidget * parent, const QStyleOptionViewItem & option,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForInteger::setEditorData(QWidget * editor,
	const QModelIndex & index) const
{
	LongLongSpinBox * valueSpinBox = (LongLongSpinBox *)editor;

	QVariant newValue = index.model()->data(index, Qt::EditRole);

	valueSpinBox->setValue(newValue.toLongLong());

	return;
}

//	END OF CC::ItemDelegateForInteger::setEditorData(QWidget * editor,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForInteger::updateEditorGeometry(QWidget * editor,
	const QStyleOptionViewItem & option, const QModelIndex & /*index*/)
	const
{
	editor->setGeometry(option.rect);
	return;
}

//	END OF void CC::ItemDelegateForInteger::updateEditorGeometry(
//		QWidget * editor, const QStyleOptionViewItem & option,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForInteger::setModelData(QWidget * editor,
	QAbstractItemModel * model, const QModelIndex & index) const
{
	LongLongSpinBox * valueSpinBox = (LongLongSpinBox *)editor;
	QVariant newValue(valueSpinBox->value());
	model->setData(index, newValue, Qt::EditRole);
	return;
}

//	END OF void CC::ItemDelegateForInteger::setModelData(QWidget * editor,
//		QAbstractItemModel * model, const QModelIndex & index) const
//==============================================================================
