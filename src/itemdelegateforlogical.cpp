#include <QComboBox>

#include "itemdelegateforlogical.h"

//==============================================================================

CC::ItemDelegateForLogical::ItemDelegateForLogical(QObject * parent):
	QStyledItemDelegate(parent)
{

}

//	END OF CC::ItemDelegateForLogical::ItemDelegateForLogical(
//		QObject * parent)
//==============================================================================

CC::ItemDelegateForLogical::~ItemDelegateForLogical()
{

}

//	END OF CC::ItemDelegateForLogical::~ItemDelegateForLogical()
//==============================================================================

QWidget * CC::ItemDelegateForLogical::createEditor(QWidget * parent,
	const QStyleOptionViewItem & /*option*/, const QModelIndex & /*index*/)
	const
{
	QComboBox * logicalComboBox = new QComboBox(parent);

	logicalComboBox->addItem(QObject::trUtf8("Yes"), QVariant(1));
	logicalComboBox->addItem(QObject::trUtf8("Unknown"), QVariant(0));
	logicalComboBox->addItem(QObject::trUtf8("No"), QVariant(-1));

	return logicalComboBox;
}

//	END OF QWidget * CC::ItemDelegateForLogical::createEditor(
//		QWidget * parent, const QStyleOptionViewItem & option,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForLogical::setEditorData(QWidget * editor,
	const QModelIndex & index) const
{
	QComboBox * logicalComboBox = (QComboBox *)editor;

	QVariant logicalValue = index.model()->data(index, Qt::EditRole);

	for(int i = 0; i < logicalComboBox->count(); ++i)
	{
		if(logicalValue == logicalComboBox->itemData(i))
		{
			logicalComboBox->setCurrentIndex(i);
			return;
		}
	}

	return;
}

//	END OF CC::ItemDelegateForLogical::setEditorData(QWidget * editor,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForLogical::updateEditorGeometry(QWidget * editor,
	const QStyleOptionViewItem & option, const QModelIndex & /*index*/)
	const
{
	editor->setGeometry(option.rect);
	return;
}

//	END OF void CC::ItemDelegateForLogical::updateEditorGeometry(
//		QWidget * editor, const QStyleOptionViewItem & option,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForLogical::setModelData(QWidget * editor,
	QAbstractItemModel * model, const QModelIndex & index) const
{
	QComboBox * logicalComboBox = (QComboBox *)editor;
	QVariant value = logicalComboBox->itemData(logicalComboBox->currentIndex());
	model->setData(index, value, Qt::EditRole);
	return;
}

//	END OF void CC::ItemDelegateForLogical::setModelData(QWidget * editor,
//		QAbstractItemModel * model, const QModelIndex & index) const
//==============================================================================
