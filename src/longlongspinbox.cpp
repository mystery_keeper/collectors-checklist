#include <QLineEdit>
#include <assert.h>
#include <stdint.h>

#include "longlongspinbox.h"

//==============================================================================

LongLongSpinBox::LongLongSpinBox(qlonglong newValue, QWidget * parent):
	QAbstractSpinBox(parent)
	, m_pSignedIntValidator(new QRegExpValidator(QRegExp("[+-]?\\d+"), this))
{
	lineEdit()->setText(QString::number(newValue));

	setAccelerated(true);
}

//	END OF LongLongSpinBox::LongLongSpinBox(qlonglong newValue,
//		QWidget * parent)
//==============================================================================

LongLongSpinBox::~LongLongSpinBox()
{

}

//	END OF LongLongSpinBox::~LongLongSpinBox()
//==============================================================================

qlonglong LongLongSpinBox::value() const
{
	return lineEdit()->text().toLongLong();
}

//	END OF qlonglong LongLongSpinBox::value() const
//==============================================================================

void LongLongSpinBox::setValue(qlonglong newValue)
{
	lineEdit()->setText(QString::number(newValue));
}

//	END OF void LongLongSpinBox::setValue(qlonglong newValue)
//==============================================================================

QValidator::State LongLongSpinBox::validate(QString & input, int & pos) const
{
	assert(m_pSignedIntValidator != nullptr);

	QValidator::State validatorState =
		m_pSignedIntValidator->validate(input, pos);

	if(validatorState == QValidator::Acceptable)
	{
		bool conversionSuccess = false;
		input.toLongLong(&conversionSuccess);
		if(conversionSuccess)
		{
			return QValidator::Acceptable;
		}
		else
		{
			return QValidator::Invalid;
		}
	}

	return validatorState;
}

//	END OF QValidator::State LongLongSpinBox::validate(QString & input,
//		int & pos) const
//==============================================================================

void LongLongSpinBox::fixup(QString & input) const
{
	qlonglong convertedValue = lineEdit()->text().toLongLong();

	QString convertedString = QString::number(convertedValue);

	if(lineEdit()->text().contains(convertedString))
	{
		input = convertedString;
	}
	else
	{
		input = QString("0");
	}
}

//	END OF void LongLongSpinBox::fixup(QString & input) const
//==============================================================================

void LongLongSpinBox::stepBy(int steps)
{
	qlonglong convertedValue = lineEdit()->text().toLongLong();

	qlonglong newValue = convertedValue + steps;

	// Check for overflow/underflow
	if((steps > 0) && (convertedValue > newValue))
	{
		newValue = INT64_MAX;
	}
	else if((steps < 0) && (convertedValue < newValue))
	{
		newValue = INT64_MIN;
	}

	lineEdit()->setText(QString::number(newValue));
}

//	END OF void LongLongSpinBox::stepBy(int steps)
//==============================================================================

QAbstractSpinBox::StepEnabled LongLongSpinBox::stepEnabled() const
{
	qlonglong convertedValue = lineEdit()->text().toLongLong();

	if(convertedValue == INT64_MAX)
	{
		return StepDownEnabled;
	}
	else if(convertedValue == INT64_MIN)
	{
		return StepUpEnabled;
	}

	return StepUpEnabled | StepDownEnabled;
}

//	END OF QAbstractSpinBox::StepEnabled LongLongSpinBox::stepEnabled() const
//==============================================================================
