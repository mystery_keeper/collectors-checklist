#include <assert.h>
#include <wchar.h>

#include "ccduration.h"

//==============================================================================

CC::Duration::Duration():
	m_days(0)
	, m_hours(0)
	, m_minutes(0)
	, m_seconds(0)
	, m_milliseconds(0)
{

}

//	END OF CC::Duration::Duration()
//==============================================================================

CC::Duration::Duration(uint64_t totalMilliseconds):
	m_days(0)
	, m_hours(0)
	, m_minutes(0)
	, m_seconds(0)
	, m_milliseconds(0)
{
	parseMilliseconds(totalMilliseconds, m_days, m_hours, m_minutes,
		m_seconds, m_milliseconds);
}

//	END OF CC::Duration::Duration(uint64_t totalMilliseconds)
//==============================================================================

CC::Duration::Duration(uint64_t days, uint8_t hours, uint8_t minutes,
	uint8_t seconds, uint16_t milliseconds):
	m_days(0)
	, m_hours(0)
	, m_minutes(0)
	, m_seconds(0)
	, m_milliseconds(0)
{
	if(!validate(days, hours, minutes, seconds, milliseconds))
	{
		return;
	}

	m_days = days;
	m_hours = hours;
	m_minutes = minutes;
	m_seconds = seconds;
	m_milliseconds = milliseconds;
}

//	END OF CC::Duration::Duration(uint64_t days, uint8_t hours, uint8_t minutes,
//		uint8_t seconds, uint16_t milliseconds)
//==============================================================================

CC::Duration::Duration(const CC::Duration& other):
	m_days(other.days())
	, m_hours(other.hours())
	, m_minutes(other.minutes())
	, m_seconds(other.seconds())
	, m_milliseconds(other.milliseconds())
{
	assert(validate(m_days, m_hours, m_minutes, m_seconds, m_milliseconds));
}

//	END OF CC::Duration::Duration(const CC::Duration& other)
//==============================================================================

CC::Duration& CC::Duration::operator=(const CC::Duration& other)
{
	if (this == &other) return *this;

	m_days = other.days();
	m_hours = other.hours();
	m_minutes = other.minutes();
	m_seconds = other.seconds();
	m_milliseconds = other.milliseconds();

	assert(validate(m_days, m_hours, m_minutes, m_seconds, m_milliseconds));

	return *this;
}

//	END OF CC::Duration& CC::Duration::operator=(const CC::Duration& other)
//==============================================================================

CC::Duration::~Duration()
{

}

//	END OF CC::Duration::~Duration()
//==============================================================================

uint64_t CC::Duration::days() const
{
	return m_days;
}

//	END OF uint64_t CC::Duration::days() const
//==============================================================================

uint8_t CC::Duration::hours() const
{
	return m_hours;
}

//	END OF uint8_t CC::Duration::hours() const
//==============================================================================

uint8_t CC::Duration::minutes() const
{
	return m_minutes;
}

//	END OF uint8_t CC::Duration::minutes() const
//==============================================================================

uint8_t CC::Duration::seconds() const
{
	return m_seconds;
}

//	END OF uint8_t CC::Duration::seconds() const
//==============================================================================

uint16_t CC::Duration::milliseconds() const
{
	return m_milliseconds;
}

//	END OF uint16_t CC::Duration::milliseconds() const
//==============================================================================

uint64_t CC::Duration::totalMilliseconds() const
{
	assert(validate(m_days, m_hours, m_minutes, m_seconds, m_milliseconds));

	return m_days * MILLISECONDS_IN_DAY + m_hours * MILLISECONDS_IN_HOUR +
		m_minutes * MILLISECONDS_IN_MINUTE + m_seconds *
		MILLISECONDS_IN_SECOND + m_milliseconds;
}

//	END OF uint64_t CC::Duration::totalMilliseconds() const
//==============================================================================

bool CC::Duration::set(uint64_t totalMilliseconds)
{
	parseMilliseconds(totalMilliseconds, m_days, m_hours, m_minutes,
		m_seconds, m_milliseconds);

	return true;
}

//	END OF bool CC::Duration::set(uint64_t totalMilliseconds)
//==============================================================================

bool CC::Duration::set(uint64_t days, uint8_t hours, uint8_t minutes,
	uint8_t seconds, uint16_t milliseconds)
{
	if(!validate(days, hours, minutes, seconds, milliseconds))
	{
		return false;
	}

	m_days = days;
	m_hours = hours;
	m_minutes = minutes;
	m_seconds = seconds;
	m_milliseconds = milliseconds;

	return true;
}

//	END OF bool CC::Duration::set(uint64_t days, uint8_t hours, uint8_t minutes,
//		uint8_t seconds, uint16_t milliseconds)
//==============================================================================

std::wstring CC::Duration::toWStringFull() const
{
	std::wstring outString(L"");
	const size_t bufferLength = 21;
	wchar_t buffer[bufferLength];

	swprintf(buffer, bufferLength, L"%llu", m_days);
	outString += buffer;
	outString += L":";
	swprintf(buffer, bufferLength, L"%.2hhu", m_hours);
	outString += buffer;
	outString += L":";
	swprintf(buffer, bufferLength, L"%.2hhu", m_minutes);
	outString += buffer;
	outString += L":";
	swprintf(buffer, bufferLength, L"%.2hhu", m_seconds);
	outString += buffer;
	outString += L".";
	swprintf(buffer, bufferLength, L"%.3hu", m_milliseconds);
	outString += buffer;

	return outString;
}

//	END OF std::wstring CC::Duration::toWStringFull() const
//==============================================================================

std::wstring CC::Duration::toWStringCompact() const
{
	std::wstring outString(L"");
	const size_t bufferLength = 21;
	wchar_t buffer[21];

	if(m_days != 0)
	{
		swprintf(buffer, bufferLength, L"%llu", m_days);
		outString += buffer;
		outString += L":";
	}

	if(m_days != 0 || m_hours != 0)
	{
		swprintf(buffer, bufferLength, L"%.2hhu", m_hours);
		outString += buffer;
		outString += L":";
	}

	swprintf(buffer, bufferLength, L"%.2hhu", m_minutes);
	outString += buffer;
	outString += L":";
	swprintf(buffer, bufferLength, L"%.2hhu", m_seconds);
	outString += buffer;

	if(m_milliseconds != 0)
	{
		outString += L".";
		swprintf(buffer, bufferLength, L"%.3hu", m_milliseconds);
		outString += buffer;
	}

	return outString;
}

//	END OF std::wstring CC::Duration::toWStringCompact() const
//==============================================================================

bool CC::Duration::validate(uint64_t days, uint8_t hours, uint8_t minutes,
	uint8_t seconds, uint16_t milliseconds)
{
	if(days > MAX_DAYS) return false;
	if(hours > 23) return false;
	if(minutes > 59) return false;
	if(seconds > 59) return false;
	if(milliseconds > 999) return false;

	// This block should never overflow.
	uint64_t sum = milliseconds;
	sum += seconds * MILLISECONDS_IN_SECOND;
	sum += minutes * MILLISECONDS_IN_MINUTE;
	sum += hours * MILLISECONDS_IN_HOUR;

	// Must check overflow with days.
	uint64_t millisecondsInDays = days * MILLISECONDS_IN_DAY;
	if(sum > (sum + millisecondsInDays))
	{
		return false;
	}

	return true;
}

//	END OF bool CC::Duration::validate(uint64_t days, uint8_t hours,
//		uint8_t minutes, uint8_t seconds, uint16_t milliseconds)
//==============================================================================

void CC::Duration::parseMilliseconds(uint64_t totalMilliseconds,
	uint64_t & days, uint8_t & hours, uint8_t & minutes, uint8_t & seconds,
	uint16_t & milliseconds)
{
	uint64_t tempMillisecods = totalMilliseconds;
	days = tempMillisecods / MILLISECONDS_IN_DAY;
	assert(days <= MAX_DAYS);
	tempMillisecods -= days * MILLISECONDS_IN_DAY;
	hours = uint8_t(tempMillisecods / MILLISECONDS_IN_HOUR);
	assert(hours <= 23u);
	tempMillisecods -= hours * MILLISECONDS_IN_HOUR;
	minutes = uint8_t(tempMillisecods / MILLISECONDS_IN_MINUTE);
	assert(minutes <= 59u);
	tempMillisecods -= minutes * MILLISECONDS_IN_MINUTE;
	seconds = uint8_t(tempMillisecods / MILLISECONDS_IN_SECOND);
	assert(seconds <= 59u);
	tempMillisecods -= seconds * MILLISECONDS_IN_SECOND;
	assert(seconds <= 999u);
	milliseconds = uint16_t(tempMillisecods);
}

//	END OF void CC::Duration::parseMilliseconds(uint64_t totalMilliseconds,
//		uint64_t & days, uint8_t & hours, uint8_t & minutes, uint8_t & seconds,
//		uint16_t & milliseconds)
//==============================================================================
