﻿#ifndef MAINWINDOW_H_INCLUDED
#define MAINWINDOW_H_INCLUDED

#include <ui_mainwindow.h>
#include <QCloseEvent>
#include <QAction>
#include <QLabel>

#include "checklistfile.h"
#include "itemdelegatefortext.h"
#include "itemdelegateforinteger.h"
#include "itemdelegateforreal.h"
#include "itemdelegateforlogical.h"
#include "itemdelegatefordate.h"
#include "itemdelegateforduration.h"
#include "fieldslisteditdialog.h"

/// Application main window.
class MainWindow final : public QMainWindow
{
	Q_OBJECT

	public:

		/// Constructor.
		MainWindow(QWidget * parent = 0, Qt::WindowFlags flags = 0);
		/// Destructor.
		~MainWindow();

	protected:

		/// Window close event handler.
		/// Reimplemented from QMainWindow.
		void closeEvent(QCloseEvent * event) override;

	private:

		/// Qt Designer generated GUI.
		Ui::MainWindow ui;

		/// Action that creates new checklist.
		QAction * m_pActionNewChecklist;
		/// Action that calls checklist open dialog.
		QAction * m_pActionOpenChecklist;
		/// Action that saves current checklist.
		QAction * m_pActionSaveChecklist;
		/// Action that calls checklist save dialog.
		QAction * m_pActionSaveChecklistAs;
		/// Action that adds new record to current checklist.
		QAction * m_pActionAddChecklistRecord;
		/// Action that deletes selected records.
		QAction * m_pActionDeleteSelectedRecords;
		/// Action that calls fields list edit dialog.
		QAction * m_pActionAdjustChecklistFields;
		/// Action that closes the application.
		QAction * m_pActionExit;
		/// Action that shows about dialog.
		QAction * m_pActionAbout;

		/// Current checklist file handler.
		CC::CheckListFile * m_pCurrentCheckListFile;

		/// View item delegate for text type item.
		CC::ItemDelegateForText * m_pItemDelegateForText;
		/// View item delegate for integer type item.
		CC::ItemDelegateForInteger * m_pItemDelegateForInteger;
		/// View item delegate for real type item.
		CC::ItemDelegateForReal * m_pItemDelegateForReal;
		/// View item delegate for logical type item.
		CC::ItemDelegateForLogical * m_pItemDelegateForLogical;
		/// View item delegate for date type item.
		CC::ItemDelegateForDate * m_pItemDelegateForDate;
		/// View item delegate for duration type item.
		CC::ItemDelegateForDuration * m_pItemDelegateForDuration;

		/// Dialog for editing checklist fields list.
		CC::FieldsListEditDialog * m_pFieldsListEditDialog;

		/// Status bar widget, showing checked and total records numbers.
		QLabel * m_pRecordsNumberLabel;

		/// Status bar spacer widget.
		QWidget * m_pSpacerWidget;

		/// Creates menu and toolbar actions.
		void createActions();

		/// Sets correct view item delegates when fields list changes.
		void resetViewItemDelegates();

		/// Prompts to save unsaved file if user action is about to discard it.
		bool promptToSaveAndConfirmDiscardFile();

		/// Destroys current checklist and replaces it with blank one.
		void recreateCurrentChecklist();

	private slots:

		/// Calls fields list edit dialog.
		void slotAdjustChecklistFields();

		/// Adds new record to current checklist.
		void slotAddChecklistRecord();

		/// Deletes selected records.
		void slotDeleteSelectedRecords();

		/// Creates new checklist.
		void slotNewChecklist();

		/// Saves current checklist.
		void slotSaveChecklist();

		/// Calls checklist save dialog.
		void slotSaveChecklistAs();

		/// Calls checklist open dialog.
		void slotOpenChecklist();

		/// Updates displayed numbers of checked and total records.
		void slotRefreshRecordsNumberLabel();

		/// Handles the change of file state or path.
		void slotFileStateOrPathChanged(CC::CheckListFile::State newState,
			const QString & newFilePath);

		/// Shows about dialog.
		void slotAbout();
};

#endif // MAINWINDOW_H_INCLUDED
