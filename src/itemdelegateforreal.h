#ifndef ITEMDELEGATEFORREAL_H_INCLUDED
#define ITEMDELEGATEFORREAL_H_INCLUDED

#include <QStyledItemDelegate>

namespace CC
{
	/// ItemView delegate for real type item.
	class ItemDelegateForReal final : public QStyledItemDelegate
	{
		Q_OBJECT

		public:

			/// Constructor.
			ItemDelegateForReal(QObject * parent = nullptr);

			/// Destructor.
			virtual ~ItemDelegateForReal();

			/// Creates editor widget for the item.
			/// Reimplemented from QStyledItemDelegate.
			virtual QWidget * createEditor(QWidget * parent,
				const QStyleOptionViewItem & option, const QModelIndex & index)
				const override;

			/// Sets data for the editor widget.
			/// Reimplemented from QStyledItemDelegate.
			virtual void setEditorData(QWidget * editor,
				const QModelIndex & index) const override;

			/// Adjusts editor size and position for the view item.
			/// Reimplemented from QStyledItemDelegate.
			virtual void updateEditorGeometry(QWidget * editor,
				const QStyleOptionViewItem & option, const QModelIndex & index)
				const override;

			/// Sets model item data from the editor.
			/// Reimplemented from QStyledItemDelegate.
			virtual void setModelData(QWidget * editor,
				QAbstractItemModel * model, const QModelIndex & index) const
				override;
	};
}

#endif // ITEMDELEGATEFORREAL_H_INCLUDED
