﻿#ifndef CHECKLISTMODEL_H_INCLUDED
#define CHECKLISTMODEL_H_INCLUDED

#include <QAbstractItemModel>
#include <QList>
#include <QMap>
#include <QBrush>

#include "ccfield.h"

namespace CC
{
	/// Checklist record.
	struct Record
	{
		/// Check flag is the heart of record, independent from all other
		/// record data.
		bool checked;

		/// User defined record data, mapped to fields by field ID.
		QMap<CC::TFieldID, QVariant> valueByFieldID;

		/// Contructor.
		Record():checked(false){}
	};

	/// Class that handles checklist and feeds it to view widget.
	/// Model itself is a list of records and can be percieved and displayed
	/// as flat table.
	/// \warning Number of columns in the table is composed of check state
	/// 	column and number of the checklist fields. Column 0 corresponds to
	/// 	check control, column 1 - to the field 0 and so on.
	class CheckListModel : public QAbstractItemModel
	{
		Q_OBJECT

		public:

			/// Constructor.
			/// \param fieldsList - The mandatory initial list of fields.
			CheckListModel(const QList<CC::Field> &fieldsList,
				QObject * parent = nullptr);

			/// Destructor.
			virtual ~CheckListModel();

			/// Mandatory reimplementation of QAbstractItemModel method.
			/// CheckListModel is flat, meaning items have no parents.
			virtual QModelIndex index(int row, int column,
				const QModelIndex & parent = QModelIndex()) const override;

			/// Mandatory reimplementation of QAbstractItemModel method.
			/// CheckListModel is flat, meaning items have no parents.
			virtual QModelIndex parent(const QModelIndex &child) const override;

			/// Mandatory reimplementation of QAbstractItemModel method.
			virtual Qt::ItemFlags flags(const QModelIndex &index) const
				override;

			/// Mandatory reimplementation of QAbstractItemModel method.
			virtual QVariant data(const QModelIndex &index,
				int role = Qt::DisplayRole) const override;

			/// Mandatory reimplementation of QAbstractItemModel method.
			virtual QVariant headerData(int section,
				Qt::Orientation orientation, int role = Qt::DisplayRole) const
				override;

			/// Mandatory reimplementation of QAbstractItemModel method.
			virtual int rowCount(const QModelIndex &parent = QModelIndex())
				const override;

			/// Mandatory reimplementation of QAbstractItemModel method.
			virtual int columnCount(const QModelIndex &parent = QModelIndex())
				const override;

			/// Sets data for the specified role for the model item, referenced
			/// by index. Reimplemented from QAbstractItemModel.
			/// \return True if data was successfully modified. False otherwise.
			virtual bool setData(const QModelIndex &index,
				const QVariant &value, int role = Qt::EditRole) override;

			/// Sets header data for the specified role for the specified
			/// header section. Reimplemented from QAbstractItemModel.
			/// \return True if data was successfully modified. False otherwise.
			virtual bool setHeaderData(int section, Qt::Orientation orientation,
				const QVariant &value, int role = Qt::EditRole) override;

			/// Sorts model records by the field, referenced by the specified
			/// column. Reimplemented from QAbstractItemModel.
			virtual void sort(int column, Qt::SortOrder order =
				Qt::AscendingOrder) override;

			/// \return Constant reference to the checklist's fields list.
			virtual const QList<CC::Field> & fieldsList() const;

			/// Modifies the fields list of the checklist.
			/// Checklist data is mapped to corresponding field by its ID and
			/// is insensible to change of field's name or type. Model will
			/// try to reuse existing data after fields list change.
			/// \return True if fields list successfully modified.
			/// 	False otherwise.
			virtual bool setFieldsList(QList<CC::Field> newFieldsList);

			/// Creates new checklist record with default field values.
			/// \return True if record was successfully created.
			/// 	False otherwise.
			virtual bool addRecord();

			/// Removes the records, enumerated in the recordNumbersList, from
			/// the checklist.
			/// \return True if records were successfully removed.
			/// 	False otherwise.
			virtual bool deleteRecords(QList<int> recordNumbersList);

			/// \return True if checklist was successfully saved to the
			/// 	specified file. False otherwise.
			virtual bool saveToFile(const QString & filePath);

			/// \return True if checklist was successfully loaded from the
			/// 	specified file. False otherwise.
			virtual bool loadFromFile(const QString & filePath);

			/// Practically the same as rowCount(), but without unneeded
			/// arguments.
			/// \return Number of records in the checklist.
			virtual int recordsNumber();

			/// \return Number of checked records in the checklist.
			virtual int checkedRecordsNumber();

		signals:

			/// Emits when number of checklist records changes.
			void recordsNumberChanged();

			/// Emits when number of checked checklist records changes.
			void checkedRecordsNumberChanged();

		private:

			/// List of checklist fields.
			QList<CC::Field> m_fieldsList;
			/// List of checklist records.
			QList<CC::Record> m_recordsList;
			/// View items background color for the checked even rows.
			QBrush m_backgroundColorCheckedEven;
			/// View items background color for the checked odd rows.
			QBrush m_backgroundColorCheckedOdd;
			/// View items background color for the unchecked even rows.
			QBrush m_backgroundColorUncheckedEven;
			/// View items background color for the unchecked odd rows.
			QBrush m_backgroundColorUncheckedOdd;

			/// Recursive merge sort function for lists of records.
			/// Used in sort() method.
			void mergeSortRecords(QList<CC::Record> &recordsList, int column,
				Qt::SortOrder order);
	};
}

#endif // CHECKLISTMODEL_H_INCLUDED
