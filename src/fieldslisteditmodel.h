#ifndef FIELDSLISTEDITMODEL_H_INCLUDED
#define FIELDSLISTEDITMODEL_H_INCLUDED

#include <QAbstractItemModel>
#include <QList>

#include "ccfield.h"

namespace CC
{
	/// Class that handles fields list and feeds it to view widget.
	/// Holds own fields list, independent from the on of the checklist.
	class FieldsListEditModel : public QAbstractItemModel
	{
		public:

			/// Constructor.
			FieldsListEditModel(QObject * parent = nullptr);

			/// Destructor.
			virtual ~FieldsListEditModel();

			/// Mandatory reimplementation of QAbstractItemModel method.
			virtual QModelIndex index(int row, int column,
				const QModelIndex & parent = QModelIndex()) const override;

			/// Mandatory reimplementation of QAbstractItemModel method.
			virtual QModelIndex parent(const QModelIndex &child) const override;

			/// Mandatory reimplementation of QAbstractItemModel method.
			virtual Qt::ItemFlags flags(const QModelIndex &index) const
				override;

			/// Mandatory reimplementation of QAbstractItemModel method.
			virtual QVariant data(const QModelIndex &index,
				int role = Qt::DisplayRole) const override;

			/// Mandatory reimplementation of QAbstractItemModel method.
			virtual QVariant headerData(int section,
				Qt::Orientation orientation, int role = Qt::DisplayRole) const
				override;

			/// Mandatory reimplementation of QAbstractItemModel method.
			virtual int rowCount(const QModelIndex &parent = QModelIndex())
				const override;

			/// Mandatory reimplementation of QAbstractItemModel method.
			virtual int columnCount(const QModelIndex &parent = QModelIndex())
				const override;

			/// Sets data for the specified role for the model item, referenced
			/// by index. Reimplemented from QAbstractItemModel.
			/// \return True if data was successfully modified. False otherwise.
			virtual bool setData(const QModelIndex &index,
				const QVariant &value, int role = Qt::EditRole) override;

			/// Sets header data for the specified role for the specified
			/// header section. Reimplemented from QAbstractItemModel.
			/// \return True if data was successfully modified. False otherwise.
			virtual bool setHeaderData(int section, Qt::Orientation orientation,
				const QVariant &value, int role = Qt::EditRole) override;

			/// \return Current fields list of the editor.
			virtual const QList<CC::Field> & fieldsList() const;

			/// Sets new fields list to the editor.
			/// \return True if fields list was set successfully.
			/// 	False otherwise.
			virtual bool setFieldsList(const QList<CC::Field> & newFieldsList);

			/// Adds new field to the editor's list.
			/// \return True if field was added successfully.
			/// 	False if all possible ID are taken.
			virtual bool addField();

			/// Deletes field from the list
			/// \return True if field was deleted successfully.
			/// 	False if field number doesn't exist.
			virtual bool deleteField(int fieldNumber);

			/// Moves field up in the list.
			/// \return True if field was moved successfully.
			/// 	False if field number doesn't exist or can not be moved.
			virtual bool moveFieldUp(int fieldNumber);

			/// Moves field down in the list.
			/// \return True if field was moved successfully.
			/// 	False if field number doesn't exist or can not be moved.
			virtual bool moveFieldDown(int fieldNumber);

		private:

			/// List of checklist fields.
			QList<CC::Field> m_fieldsList;
	};
}

#endif // FIELDSLISTEDITMODEL_H_INCLUDED
