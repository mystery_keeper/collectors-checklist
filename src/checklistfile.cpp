#include <QStandardPaths>
#include <QCoreApplication>
#include <QFileInfo>
#include <assert.h>

#include "checklistfile.h"

//==============================================================================

CC::CheckListFile::CheckListFile(const QList<CC::Field> &fieldsList,
	QString filePath, QObject * parent) : QObject(parent)
	, m_pModel(nullptr)
	, m_state(CC::CheckListFile::State::New)
	, m_filePath(filePath)
{
	m_pModel = new CC::CheckListModel(fieldsList, this);
	assert(m_pModel != nullptr);

	m_pModel->addRecord();

	/// \cond
	connect(m_pModel, SIGNAL(columnsInserted(const QModelIndex &, int, int)),
		this, SLOT(slotSetFileChanged()));
	connect(m_pModel, SIGNAL(columnsMoved(const QModelIndex &, int, int,
		const QModelIndex &, int)),
		this, SLOT(slotSetFileChanged()));
	connect(m_pModel, SIGNAL(columnsRemoved(const QModelIndex &, int, int)),
		this, SLOT(slotSetFileChanged()));
	connect(m_pModel, SIGNAL(dataChanged(const QModelIndex &,
		const QModelIndex &, const QVector<int> &)),
		this, SLOT(slotSetFileChanged()));
	connect(m_pModel, SIGNAL(headerDataChanged(Qt::Orientation, int, int)),
		this, SLOT(slotSetFileChanged()));
	connect(m_pModel, SIGNAL(layoutChanged(const QList<QPersistentModelIndex> &,
		QAbstractItemModel::LayoutChangeHint)),
		this, SLOT(slotSetFileChanged()));
	connect(m_pModel, SIGNAL(modelReset()),
		this, SLOT(slotSetFileChanged()));
	connect(m_pModel, SIGNAL(rowsInserted(const QModelIndex &, int, int)),
		this, SLOT(slotSetFileChanged()));
	connect(m_pModel, SIGNAL(rowsMoved(const QModelIndex &, int, int,
		const QModelIndex &, int)),
		this, SLOT(slotSetFileChanged()));
	connect(m_pModel, SIGNAL(rowsRemoved(const QModelIndex &, int, int)),
		this, SLOT(slotSetFileChanged()));
	/// \endcond

	if(m_filePath.isEmpty())
	{
		// Default path is user documents dir.
		m_filePath = QStandardPaths::writableLocation(
			QStandardPaths::StandardLocation::DocumentsLocation);

		if(m_filePath.isEmpty())
		{
			// If failed to get the documents dir, default to application dir.
			m_filePath = QCoreApplication::applicationDirPath();
			m_filePath += "/checklists";
		}

		m_filePath += "/New checklist.checklist";
	}
}

//	END OF CC::CheckListFile::CheckListFile(const QList<CC::Field> &fieldsList,
//		QString filePath, QObject * parent)
//==============================================================================

CC::CheckListFile:: ~CheckListFile()
{

}

//	END OF CC::CheckListFile:: ~CheckListFile()
//==============================================================================

CC::CheckListModel * CC::CheckListFile::model() const
{
	return m_pModel;
}

//	END OF CC::CheckListModel * CC::CheckListFile::model()
//==============================================================================

CC::CheckListFile::State CC::CheckListFile::state() const
{
	return m_state;
}

//	END OF CC::CheckListFile::State CC::CheckListFile::state() const
//==============================================================================

QString CC::CheckListFile::name() const
{
	QFileInfo tempFileInfo(m_filePath);
	return tempFileInfo.fileName();
}

//	END OF QString CC::CheckListFile::name() const
//==============================================================================

const QString & CC::CheckListFile::filePath() const
{
	return m_filePath;
}

//	END OF const QString & CC::CheckListFile::filePath() const
//==============================================================================

bool CC::CheckListFile::save()
{
	if(m_filePath.isEmpty()) return false;

	bool savedSuccesfully = m_pModel->saveToFile(m_filePath);

	if(savedSuccesfully && (m_state != CC::CheckListFile::State::Unchanged))
	{
		m_state = CC::CheckListFile::State::Unchanged;
		emit signalStateOrPathChanged(m_state, m_filePath);
	}

	return savedSuccesfully;
}

//	END OF bool CC::CheckListFile::save()
//==============================================================================

bool CC::CheckListFile::saveToFileAndRename(const QString & filePath)
{
	if(filePath == m_filePath)
	{
		return save();
	}

	// else

	bool savedSuccesfully = m_pModel->saveToFile(filePath);

	if(savedSuccesfully)
	{
		m_filePath = filePath;
		m_state = CC::CheckListFile::State::Unchanged;
		emit signalStateOrPathChanged(m_state, m_filePath);
	}

	return savedSuccesfully;
}

//	END OF bool CC::CheckListFile::saveToFileAndRename(const QString & filePath)
//==============================================================================

bool CC::CheckListFile::loadFromFileAndRename(const QString & filePath)
{
	bool loadedSuccessfully = m_pModel->loadFromFile(filePath);

	if(loadedSuccessfully)
	{
		m_filePath = filePath;
		m_state = CC::CheckListFile::State::Unchanged;
		emit signalStateOrPathChanged(m_state, m_filePath);
	}

	return loadedSuccessfully;
}

//	END OF bool CC::CheckListFile::loadFromFileAndRename(
//		const QString & filePath)
//==============================================================================

void CC::CheckListFile::slotSetFileChanged()
{
	if(m_state == CC::CheckListFile::State::New)
	{
		m_state = CC::CheckListFile::State::NewAndChanged;
		emit signalStateOrPathChanged(m_state, m_filePath);
	}
	else if(m_state == CC::CheckListFile::State::Unchanged)
	{
		m_state = CC::CheckListFile::State::Changed;
		emit signalStateOrPathChanged(m_state, m_filePath);
	}
}

//	END OF void CC::CheckListFile::slotSetFileChanged()
//==============================================================================
