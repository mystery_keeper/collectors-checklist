#include <QDoubleSpinBox>

#include <float.h>
#include "itemdelegateforreal.h"

//==============================================================================

CC::ItemDelegateForReal::ItemDelegateForReal(QObject * parent):
	QStyledItemDelegate(parent)
{

}

//	END OF CC::ItemDelegateForReal::ItemDelegateForReal(
//		QObject * parent)
//==============================================================================

CC::ItemDelegateForReal::~ItemDelegateForReal()
{

}

//	END OF CC::ItemDelegateForReal::~ItemDelegateForReal()
//==============================================================================

QWidget * CC::ItemDelegateForReal::createEditor(QWidget * parent,
	const QStyleOptionViewItem & /*option*/, const QModelIndex & /*index*/)
	const
{
	QDoubleSpinBox * valueSpinBox = new QDoubleSpinBox(parent);
	valueSpinBox->setDecimals(DBL_DIG);
	valueSpinBox->setRange(-DBL_MAX, DBL_MAX);

	return valueSpinBox;
}

//	END OF QWidget * CC::ItemDelegateForReal::createEditor(
//		QWidget * parent, const QStyleOptionViewItem & option,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForReal::setEditorData(QWidget * editor,
	const QModelIndex & index) const
{
	QDoubleSpinBox * valueSpinBox = (QDoubleSpinBox *)editor;

	QVariant newValue = index.model()->data(index, Qt::EditRole);

	valueSpinBox->setValue(newValue.toReal());

	return;
}

//	END OF CC::ItemDelegateForReal::setEditorData(QWidget * editor,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForReal::updateEditorGeometry(QWidget * editor,
	const QStyleOptionViewItem & option, const QModelIndex & /*index*/)
	const
{
	editor->setGeometry(option.rect);
	return;
}

//	END OF void CC::ItemDelegateForReal::updateEditorGeometry(
//		QWidget * editor, const QStyleOptionViewItem & option,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForReal::setModelData(QWidget * editor,
	QAbstractItemModel * model, const QModelIndex & index) const
{
	QDoubleSpinBox * valueSpinBox = (QDoubleSpinBox *)editor;
	QVariant newValue(valueSpinBox->value());
	model->setData(index, newValue, Qt::EditRole);
	return;
}

//	END OF void CC::ItemDelegateForReal::setModelData(QWidget * editor,
//		QAbstractItemModel * model, const QModelIndex & index) const
//==============================================================================
