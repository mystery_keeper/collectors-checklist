﻿#ifndef CCFIELD_H_INCLUDED
#define CCFIELD_H_INCLUDED

#include <string>
#include <stdint.h>

namespace CC
{
	typedef uint64_t TFieldID;
	static const TFieldID FIELD_ID_MAX = UINT64_MAX;

	typedef uint_fast8_t TFieldType;

	enum FieldType : TFieldType
	{
		Text = 0
		, Integer = 1
		, Real = 2
		, Logical = 3 ///< Yes, No, Unknown.
		, Date = 4
		, Time = 5 ///< May be daytime OR duration.
	};

	/// Checklist field descriptor.
	struct Field
	{
		CC::TFieldID id; ///< Plain counter ID.
		CC::FieldType type;
		std::wstring name;
	};
}

#endif // CCFIELD_H_INCLUDED
