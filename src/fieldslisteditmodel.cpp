#include <assert.h>

#include "fieldslisteditmodel.h"

//==============================================================================

CC::FieldsListEditModel::FieldsListEditModel(QObject * parent):
	QAbstractItemModel(parent)
{

}

//	END OF CC::FieldsListEditModel::FieldsListEditModel(QObject * parent)
//==============================================================================

CC::FieldsListEditModel::~FieldsListEditModel()
{

}

//	END OF CC::FieldsListEditModel::~FieldsListEditModel()
//==============================================================================

QModelIndex CC::FieldsListEditModel::index(int row, int column,
	const QModelIndex & /*parent*/) const
{
	return createIndex(row, column);
}

//	END OF QModelIndex CC::FieldsListEditModel::index(int row, int column,
//		const QModelIndex & parent) const
//==============================================================================

QModelIndex CC::FieldsListEditModel::parent(const QModelIndex & /*child*/) const
{
	return QModelIndex();
}

//	END OF QModelIndex CC::FieldsListEditModel::parent(const QModelIndex &child)
//		const
//==============================================================================

Qt::ItemFlags CC::FieldsListEditModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
	{
		return Qt::NoItemFlags;
	}

	Qt::ItemFlags f = Qt::NoItemFlags
		| Qt::ItemIsEnabled
		| Qt::ItemIsSelectable
		| Qt::ItemIsEditable
	;

	return f;
}

//	END OF Qt::ItemFlags CC::FieldsListEditModel::flags(
//		const QModelIndex &index) const
//==============================================================================

QVariant CC::FieldsListEditModel::data(const QModelIndex &index, int role) const
{
	if(!index.isValid())
	{
		return QVariant();
	}

	if((index.row() >= m_fieldsList.size()) | (index.column() >= 2))
	{
		return QVariant();
	}

	if(index.column() == 0)
	{
		if(role == Qt::DisplayRole || role == Qt::ToolTipRole ||
			role == Qt::EditRole)
		{
			return QVariant(QString::fromStdWString(
				m_fieldsList[index.row()].name));
		}
	}
	else if(index.column() == 1)
	{
		if(role == Qt::DisplayRole || role == Qt::ToolTipRole)
		{
			if(m_fieldsList[index.row()].type == CC::FieldType::Text)
			{
				return QVariant(QObject::trUtf8("Text"));
			}
			else if(m_fieldsList[index.row()].type == CC::FieldType::Integer)
			{
				return QVariant(QObject::trUtf8("Integer"));
			}
			else if(m_fieldsList[index.row()].type == CC::FieldType::Real)
			{
				return QVariant(QObject::trUtf8("Real"));
			}
			else if(m_fieldsList[index.row()].type == CC::FieldType::Logical)
			{
				return QVariant(QObject::trUtf8("Logical"));
			}
			else if(m_fieldsList[index.row()].type == CC::FieldType::Date)
			{
				return QVariant(QObject::trUtf8("Date"));
			}
			else if(m_fieldsList[index.row()].type == CC::FieldType::Time)
			{
				return QVariant(QObject::trUtf8("Time"));
			}
		}
		else if(role == Qt::EditRole)
		{
			if(m_fieldsList[index.row()].type == CC::FieldType::Text)
			{
				return QVariant(CC::FieldType::Text);
			}
			else if(m_fieldsList[index.row()].type == CC::FieldType::Integer)
			{
				return QVariant(CC::FieldType::Integer);
			}
			else if(m_fieldsList[index.row()].type == CC::FieldType::Real)
			{
				return QVariant(CC::FieldType::Real);
			}
			else if(m_fieldsList[index.row()].type == CC::FieldType::Logical)
			{
				return QVariant(CC::FieldType::Logical);
			}
			else if(m_fieldsList[index.row()].type == CC::FieldType::Date)
			{
				return QVariant(CC::FieldType::Date);
			}
			else if(m_fieldsList[index.row()].type == CC::FieldType::Time)
			{
				return QVariant(CC::FieldType::Time);
			}
		}
	}

	return QVariant();
}

//	END OF QVariant FieldsListEditModel::data(const QModelIndex &index,
//		int role) const
//==============================================================================

QVariant CC::FieldsListEditModel::headerData(int section,
	Qt::Orientation orientation, int role) const
{
	if(section >= 2 || orientation != Qt::Horizontal)
	{
		return QVariant();
	}

	if(section == 0)
	{
		if(role == Qt::DisplayRole)
		{
			return QVariant(QObject::trUtf8("Title"));
		}
		else if(role == Qt::ToolTipRole)
		{
			return QVariant(QObject::trUtf8("Field's title."));
		}
	}
	else if(section == 1)
	{
		if(role == Qt::DisplayRole)
		{
			return QVariant(QObject::trUtf8("Type"));
		}
		else if(role == Qt::ToolTipRole)
		{
			return QVariant(QObject::trUtf8("Field's type."));
		}
	}


	return QVariant();
}

//	END OF QVariant CC::FieldsListEditModel::headerData(int section,
//		Qt::Orientation orientation, int role) const
//==============================================================================

int CC::FieldsListEditModel::rowCount(const QModelIndex & /*parent*/) const
{
	return m_fieldsList.size();
}

//	END OF int CC::FieldsListEditModel::rowCount(const QModelIndex &parent)
//		const
//==============================================================================

int CC::FieldsListEditModel::columnCount(const QModelIndex & /*parent*/) const
{
	return 2;
}

//	END OF int CC::FieldsListEditModel::columnCount(const QModelIndex &parent)
//		const
//==============================================================================

bool CC::FieldsListEditModel::setData(const QModelIndex &index,
	const QVariant &value, int role)
{
	if(index.column() >= 2 || index.row() >= m_fieldsList.size())
	{
		return false;
	}

	if(index.column() == 0)
	{
		if(role == Qt::EditRole)
		{
			m_fieldsList[index.row()].name = value.toString().toStdWString();
			return true;
		}
	}
	else if(index.column() == 1)
	{
		if(role == Qt::EditRole)
		{
			if(
				value.toUInt() != CC::FieldType::Text &&
				value.toUInt() != CC::FieldType::Integer &&
				value.toUInt() != CC::FieldType::Real &&
				value.toUInt() != CC::FieldType::Logical &&
				value.toUInt() != CC::FieldType::Date &&
				value.toUInt() != CC::FieldType::Time
			)
			{
				return false;
			}

			m_fieldsList[index.row()].type = CC::FieldType(value.toUInt());
			return true;
		}
	}

	return false;
}

//	END OF bool CC::FieldsListEditModel::setData(const QModelIndex &index,
//		const QVariant &value, int role)
//==============================================================================

bool CC::FieldsListEditModel::setHeaderData(int /*section*/,
	Qt::Orientation /*orientation*/, const QVariant & /*value*/, int /*role*/)
{
	return false;
}

//	END OF bool CC::FieldsListEditModel::setHeaderData(int section,
//		Qt::Orientation orientation, const QVariant &value, int role)
//==============================================================================

const QList<CC::Field> & CC::FieldsListEditModel::fieldsList() const
{
	return m_fieldsList;
}

//	END OF const QList<CC::Field> & QList<CC::Field>
//		CC::FieldsListEditModel::fieldsList() const
//==============================================================================

bool CC::FieldsListEditModel::setFieldsList(
	const QList<CC::Field> & newFieldsList)
{
	if(newFieldsList.empty())
	{
		return false;
	}

	emit layoutAboutToBeChanged();

	m_fieldsList = newFieldsList;

	emit layoutChanged();
	return true;
}

//	END OF bool CC::FieldsListEditModel::setFieldsList(
//		const QList<CC::Field> & newFieldsList)
//==============================================================================

bool CC::FieldsListEditModel::addField()
{
	CC::TFieldID newID = 0;
	bool idExists = false;

	while(true)
	{
		idExists = false;

		for(int i = 0; i < m_fieldsList.size(); ++i)
		{
			if(m_fieldsList[i].id == newID)
			{
				idExists = true;
				break;
			}
		}

		if(idExists)
		{
			if(newID == CC::FIELD_ID_MAX)
			{
				return false;
			}

			newID++;
		}
		else
		{
			break;
		}
	}

	CC::Field newField;
	newField.id = newID;
	newField.name = L"New field";
	newField.type = CC::FieldType::Text;

	int newRowNumber = m_fieldsList.size();
	beginInsertRows(QModelIndex(), newRowNumber, newRowNumber);

	m_fieldsList.append(newField);

	endInsertRows();

	return true;
}

//	END OF bool CC::FieldsListEditModel::addField()
//==============================================================================

bool CC::FieldsListEditModel::deleteField(int fieldNumber)
{
	if(fieldNumber < 0 || fieldNumber >= m_fieldsList.size())
	{
		return false;
	}

	beginRemoveRows(QModelIndex(), fieldNumber, fieldNumber);
	m_fieldsList.removeAt(fieldNumber);
	endRemoveRows();

	return true;
}

//	END OF bool CC::FieldsListEditModel::deleteField(int fieldNumber)
//==============================================================================

bool CC::FieldsListEditModel::moveFieldUp(int fieldNumber)
{
	if(fieldNumber < 1 || fieldNumber >= m_fieldsList.size())
	{
		return false;
	}

	m_fieldsList.swap(fieldNumber - 1, fieldNumber);
	emit dataChanged(createIndex(fieldNumber - 1, 0),
		createIndex(fieldNumber, 1));

	return true;
}

//	END OF bool CC::FieldsListEditModel::moveFieldUp(int fieldNumber)
//==============================================================================

bool CC::FieldsListEditModel::moveFieldDown(int fieldNumber)
{
	if(fieldNumber < 0 || fieldNumber >= m_fieldsList.size() - 1)
	{
		return false;
	}

	m_fieldsList.swap(fieldNumber, fieldNumber + 1);
	emit dataChanged(createIndex(fieldNumber, 0),
		createIndex(fieldNumber + 1, 1));

	return true;
}

//	END OF bool CC::FieldsListEditModel::moveFieldDown(int fieldNumber)
//==============================================================================
