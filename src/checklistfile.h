#ifndef CHECKLISTFILE_H_INCLUDED
#define CHECKLISTFILE_H_INCLUDED

#include <QString>

#include "checklistmodel.h"

namespace CC
{
	/// Class that manages checklist, associates it with file path and monitors
	/// the changes.
	class CheckListFile : public QObject
	{
		Q_OBJECT

		public:

			/// Constructor.
			/// \param fieldsList - The mandatory initial list of fields.
			/// 	Used for CheckListModel creation.
			/// \param filePath - Initial file path.
			CheckListFile(const QList<CC::Field> &fieldsList,
				QString filePath = "", QObject * parent = nullptr);

			/// Destructor.
			virtual ~CheckListFile();

			/// \return Pointer to the checklist model that can be used by
			/// 	view widget and associated objects.
			CC::CheckListModel * model() const;

			/// Checklist file states.
			enum State : uint_fast8_t
			{
				New
				///< Just created. Physical file considered non existent.
				, NewAndChanged
				///< Was changed, but physical file considered non existent.
				, Unchanged
				///< Checklist was loaded from or saved to physical file.
				, Changed
				///< Checklist was loaded from or saved to physical file and
				///< then changed.
			};

			/// \return Current state of file.
			CC::CheckListFile::State state() const;

			/// \return Short file name.
			QString name() const;

			/// \return Full file path.
			const QString & filePath() const;

			/// Save checklist to currently associated file.
			/// \return True if file was saved successfully. False otherwise.
			virtual bool save();

			/// Save checklist to the specified file and reassociate.
			/// \return True if file was saved successfully. False otherwise.
			virtual bool saveToFileAndRename(const QString & filePath);

			/// Load checklist from the specified file and reassociate.
			/// \return True if file was loaded successfully. False otherwise.
			virtual bool loadFromFileAndRename(const QString & filePath);

		public slots:

			/// Handles changes in checklist.
			virtual void slotSetFileChanged();

		signals:

			/// Signals about changes in file state of path.
			void signalStateOrPathChanged(CC::CheckListFile::State newState,
				const QString & newFilePath);

		private:

			/// Checklist model, associated with the file path.
			CC::CheckListModel * m_pModel;

			/// Checklist file state.
			CC::CheckListFile::State m_state;

			/// File path, associated with the checklist.
			QString m_filePath;
	};
}


#endif // CHECKLISTFILE_H_INCLUDED
