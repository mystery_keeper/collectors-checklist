#include <QLineEdit>
#include <assert.h>

#include "durationspinbox.h"

//==============================================================================

DurationSpinBox::DurationSpinBox(QWidget * parent):
	QAbstractSpinBox(parent)
{
	m_acceptableCharactersSet.insert(QChar('0'));
	m_acceptableCharactersSet.insert(QChar('1'));
	m_acceptableCharactersSet.insert(QChar('2'));
	m_acceptableCharactersSet.insert(QChar('3'));
	m_acceptableCharactersSet.insert(QChar('4'));
	m_acceptableCharactersSet.insert(QChar('5'));
	m_acceptableCharactersSet.insert(QChar('6'));
	m_acceptableCharactersSet.insert(QChar('7'));
	m_acceptableCharactersSet.insert(QChar('8'));
	m_acceptableCharactersSet.insert(QChar('9'));
	m_acceptableCharactersSet.insert(QChar(':'));
	m_acceptableCharactersSet.insert(QChar('.'));

	lineEdit()->setText(QString("0:00:00:00.000"));

	setAccelerated(true);
}

//	END OF DurationSpinBox::DurationSpinBox(QWidget * parent)
//==============================================================================

DurationSpinBox::~DurationSpinBox()
{

}

//	END OF DurationSpinBox::~DurationSpinBox()
//==============================================================================

qulonglong DurationSpinBox::value() const
{
	return textToDuration().totalMilliseconds();
}

//	END OF qulonglong DurationSpinBox::value() const
//==============================================================================

void DurationSpinBox::setValue(qulonglong newValue)
{
	CC::Duration tempDuration;
	tempDuration.set(newValue);
	QString tempString = QString::fromStdWString(tempDuration.toWStringFull());
	int pos = 0;
	if(validate(tempString, pos) == QValidator::Acceptable)
	{
		lineEdit()->setText(tempString);
	}
}

//	END OF void DurationSpinBox::setValue(qulonglong newValue)
//==============================================================================

QValidator::State DurationSpinBox::validate(QString & input,
	int & /*pos*/) const
{
	if(input.isEmpty())
	{
		return QValidator::Intermediate;
	}

	// Check whole string for invalid characters.
	for(int i = 0; i < input.size(); ++i)
	{
		if(!m_acceptableCharactersSet.contains(input[i]))
		{
			return QValidator::Invalid;
		}
	}

	// Separate milliseconds if dot is present.
	QStringList stringSplitAtDot = input.split('.');

	// More than one dot is invalid.
	if(stringSplitAtDot.size() > 2)
	{
		return QValidator::Invalid;
	}

	// Separate days, hours, minutes and seconds.
	QStringList stringSplitAtDoubleDot = stringSplitAtDot[0].split(':');

	// More than 4 sections is invalid.
	if(stringSplitAtDoubleDot.size() > 4)
	{
		return QValidator::Invalid;
	}

	uint64_t days = 0;
	uint8_t hours = 0;
	uint8_t minutes = 0;
	uint8_t seconds = 0;
	uint16_t milliseconds = 0;

	bool conversionSuccessful = false;

	// Section lengths checks.
	for(int i = 0; i < stringSplitAtDoubleDot.size(); ++i)
	{
		if(stringSplitAtDoubleDot[i].isEmpty())
		{
			return QValidator::Intermediate;
		}

		if(stringSplitAtDoubleDot[i].size() > 12)
		{
			return QValidator::Invalid;
		}

		if((i != 0) && (stringSplitAtDoubleDot[i].size() > 2))
		{
			return QValidator::Invalid;
		}
	}

	if((stringSplitAtDoubleDot[0].size() > 2) &&
		(stringSplitAtDoubleDot.size() != 4))
	{
		return QValidator::Intermediate;
	}

	// Check after dot.
	if(stringSplitAtDot.size() > 1)
	{
		if(stringSplitAtDot[1].isEmpty())
		{
			return QValidator::Intermediate;
		}
		else if(stringSplitAtDot[1].size() > 3)
		{
			return QValidator::Invalid;
		}
		else
		{
			while(stringSplitAtDot[1].size() < 3)
			{
				stringSplitAtDot[1].append('0');
			}

			milliseconds = stringSplitAtDot[1].toUShort(&conversionSuccessful);
			if(!conversionSuccessful)
			{
				return QValidator::Invalid;
			}
		}
	}

	int sections = stringSplitAtDoubleDot.size();

	seconds = (uint8_t)stringSplitAtDoubleDot[sections - 1]
		.toUInt(&conversionSuccessful);
	if(!conversionSuccessful)
	{
		return QValidator::Invalid;
	}

	if(sections > 1)
	{
		minutes =(uint8_t)stringSplitAtDoubleDot[sections - 2]
			.toUInt(&conversionSuccessful);
			if(!conversionSuccessful)
		{
			return QValidator::Invalid;
		}
	}

	if(sections > 2)
	{
		hours =(uint8_t)stringSplitAtDoubleDot[sections - 3]
			.toUInt(&conversionSuccessful);
			if(!conversionSuccessful)
		{
			return QValidator::Invalid;
		}
	}

	if(sections > 3)
	{
		days = stringSplitAtDoubleDot[sections - 4]
			.toULongLong(&conversionSuccessful);
			if(!conversionSuccessful)
		{
			return QValidator::Invalid;
		}
	}

	if(CC::Duration::validate(days, hours, minutes, seconds, milliseconds))
	{
		return QValidator::Acceptable;
	}

	return QValidator::Invalid;
}

//	END OF QValidator::State DurationSpinBox::validate(QString & input,
//		int & pos) const
//==============================================================================

void DurationSpinBox::fixup(QString & input) const
{
	input = QString("0:00:00:00.000");
}

//	END OF void DurationSpinBox::fixup(QString & input) const
//==============================================================================

void DurationSpinBox::stepBy(int steps)
{
	if(steps == 0)
	{
		return;
	}

	CC::Duration currentDuration = textToDuration();
	qlonglong millisecondsToAdd = 0ll;

	int cursorPosition = 0;

	if(lineEdit()->hasFocus())
	{
		cursorPosition = lineEdit()->cursorPosition();
		int dotsOnLeft = 0;
		int doubleDotsOnRight = 0;

		QString timeString = lineEdit()->text();

		for(int i = 0; i < cursorPosition; ++i)
		{
			if(timeString[i] == '.')
			{
				++dotsOnLeft;
			}
		}

		for(int i = cursorPosition; i < timeString.size(); ++i)
		{
			if(timeString[i] == ':')
			{
				++doubleDotsOnRight;
			}
		}

		if(dotsOnLeft == 1)
		{
			millisecondsToAdd = (qlonglong)steps;
		}
		else if(doubleDotsOnRight == 0)
		{
			millisecondsToAdd = (qlonglong)steps *
				(qlonglong)CC::Duration::MILLISECONDS_IN_SECOND;
		}
		else if(doubleDotsOnRight == 1)
		{
			millisecondsToAdd = (qlonglong)steps *
				(qlonglong)CC::Duration::MILLISECONDS_IN_MINUTE;
		}
		else if(doubleDotsOnRight == 2)
		{
			millisecondsToAdd = (qlonglong)steps *
				(qlonglong)CC::Duration::MILLISECONDS_IN_HOUR;
		}
		else if(doubleDotsOnRight == 3)
		{
			millisecondsToAdd = (qlonglong)steps *
				(qlonglong)CC::Duration::MILLISECONDS_IN_DAY;
		}
	}
	else
	{
		millisecondsToAdd = (qlonglong)steps;
	}

	qulonglong newMilliseconds = currentDuration.totalMilliseconds();

	newMilliseconds += millisecondsToAdd;

	if((steps > 0) && (newMilliseconds <
		currentDuration.totalMilliseconds())) // Overflow.
	{
		newMilliseconds = 0xFFFFFFFFFFFFFFFFull;
	}
	else if((steps < 0) && (newMilliseconds >
		currentDuration.totalMilliseconds())) // Underflow.
	{
		newMilliseconds = 0ull;
	}

	CC::Duration newDuration(newMilliseconds);

	lineEdit()->setText(QString::fromStdWString(newDuration.toWStringFull()));

	if(lineEdit()->hasFocus())
	{
		lineEdit()->setCursorPosition(cursorPosition);
	}
}

//	END OF void DurationSpinBox::stepBy(int steps)
//==============================================================================

QAbstractSpinBox::StepEnabled DurationSpinBox::stepEnabled() const
{
	qulonglong milliseconds = textToDuration().totalMilliseconds();

	if(milliseconds == 0ull)
	{
		return StepUpEnabled;
	}
	else if(milliseconds == 0xFFFFFFFFFFFFFFFFull)
	{
		return StepDownEnabled;
	}

	return StepUpEnabled | StepDownEnabled;
}

//	END OF QAbstractSpinBox::StepEnabled DurationSpinBox::stepEnabled() const
//==============================================================================

CC::Duration DurationSpinBox::textToDuration() const
{
	QStringList stringSplitAtDot = lineEdit()->text().split('.');

	QStringList stringSplitAtDoubleDot = stringSplitAtDot[0].split(':');

	uint64_t days = 0;
	uint8_t hours = 0;
	uint8_t minutes = 0;
	uint8_t seconds = 0;
	uint16_t milliseconds = 0;

	if(stringSplitAtDot.size() > 1)
	{
		while(stringSplitAtDot[1].size() < 3)
		{
			stringSplitAtDot[1].append('0');
		}
		milliseconds = stringSplitAtDot[1].toUShort();
	}

	int sections = stringSplitAtDoubleDot.size();

	seconds = (uint8_t)stringSplitAtDoubleDot[sections - 1].toUInt();

	if(sections > 1)
	{
		minutes =(uint8_t)stringSplitAtDoubleDot[sections - 2].toUInt();
	}

	if(sections > 2)
	{
		hours =(uint8_t)stringSplitAtDoubleDot[sections - 3].toUInt();
	}

	if(sections > 3)
	{
		days = stringSplitAtDoubleDot[sections - 4].toULongLong();
	}

	CC::Duration tempDuration;
	tempDuration.set(days, hours, minutes, seconds, milliseconds);

	return tempDuration;
}

//	END OF CC::Duration QAbstractSpinBox::textToMilliseconds() const
//==============================================================================
