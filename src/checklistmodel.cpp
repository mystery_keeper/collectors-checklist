﻿#include <QDate>
#include <QTime>
#include <QSize>
#include <QIcon>
#include <assert.h>
#include <float.h>
#include <QFile>
#include <QSet>
#include <string>

#include "pugixml/pugixml.hpp"
#include "ccduration.h"
#include "checklistmodel.h"

//==============================================================================

CC::CheckListModel::CheckListModel(const QList<CC::Field> &fieldsList,
	QObject * parent):
	QAbstractItemModel(parent)
	, m_fieldsList(fieldsList)
	, m_backgroundColorCheckedEven(QColor(213, 255, 213))
	, m_backgroundColorCheckedOdd(QColor(180, 255, 180))
	, m_backgroundColorUncheckedEven(QColor(255, 255, 213))
	, m_backgroundColorUncheckedOdd(QColor(255, 255, 180))
{
	assert(!fieldsList.empty());
	/// \todo Implement color selection in settings.
}

//	END OF CC::CheckListModel::CheckListModel(const QList<CCField> &fieldsList,
//		QObject * parent)
//==============================================================================

CC::CheckListModel::~CheckListModel()
{

}

//	END OF CC::CheckListModel::~CheckListModel()
//==============================================================================

QModelIndex CC::CheckListModel::index(int row, int column,
	const QModelIndex & /*parent*/) const
{
	return createIndex(row, column);
}

//	END OF QModelIndex CC::CheckListModel::index(int row, int column,
//		const QModelIndex & parent) const
//==============================================================================

QModelIndex CC::CheckListModel::parent(const QModelIndex & /*child*/) const
{
	return QModelIndex();
}

//	END OF QModelIndex CC::CheckListModel::parent(const QModelIndex & child)
//		const
//==============================================================================

Qt::ItemFlags CC::CheckListModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
	{
		return Qt::NoItemFlags;
	}

	// For all columns
	Qt::ItemFlags f = Qt::NoItemFlags
		| Qt::ItemIsEnabled
		| Qt::ItemIsSelectable
	;

	if(index.column() == 0) // Checks column
	{
		f = f
			| Qt::ItemIsUserCheckable
		;
	}
	else // All other columns
	{
		f = f
			| Qt::ItemIsEditable
		;
	}

	return f;
}

//	END OF Qt::ItemFlags CC::CheckListModel::flags(const QModelIndex &index)
//		const
//==============================================================================

QVariant CC::CheckListModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
	{
		return QVariant();
	}

	if(index.column() >= m_fieldsList.size() + 1 ||
		index.row() >= m_recordsList.size())
	{
		return QVariant();
	}

	// Backgroud color alters for odd and even lines and depends on the
	// check state of the record.
	if(role == Qt::BackgroundRole)
	{
		if((index.row() & int(1)) == 0)
		{
			if(m_recordsList[index.row()].checked)
			{
				return(QVariant(m_backgroundColorCheckedEven));
			}
			else
			{
				return(QVariant(m_backgroundColorUncheckedEven));
			}
		}
		else
		{
			if(m_recordsList[index.row()].checked)
			{
				return(QVariant(m_backgroundColorCheckedOdd));
			}
			else
			{
				return(QVariant(m_backgroundColorUncheckedOdd));
			}
		}
	}

	if(index.column() == 0)
	{
		// Column 0 corresponds to the checkbox for the record state.

		if(role == Qt::CheckStateRole)
		{
			if(m_recordsList[index.row()].checked)
			{
				return QVariant(Qt::Checked);
			}
			else
			{
				return QVariant(Qt::Unchecked);
			}
		}
		else if(role == Qt::ToolTipRole)
		{
			if(m_recordsList[index.row()].checked)
			{
				return QVariant(QObject::trUtf8("Check"));
			}
			else
			{
				return QVariant(QObject::trUtf8("No check"));
			}
		}
	}
	else
	{
		CC::TFieldID fieldID = m_fieldsList[index.column() - 1].id;
		CC::TFieldType fieldType = m_fieldsList[index.column() - 1].type;

		if(role == Qt::DisplayRole || role == Qt::ToolTipRole)
		{
			QString stringToDisplay;

			if(fieldType == CC::FieldType::Text)
			{
				stringToDisplay = m_recordsList[index.row()].
					valueByFieldID[fieldID].toString();

				if(role == Qt::DisplayRole)
				{
					stringToDisplay = stringToDisplay.simplified();
				}
			}
			else if(fieldType == CC::FieldType::Integer)
			{
				stringToDisplay = QString::number(m_recordsList[index.row()].
					valueByFieldID[fieldID].toLongLong());
			}
			else if(fieldType == CC::FieldType::Real)
			{
				stringToDisplay = QString::number(m_recordsList[index.row()].
					valueByFieldID[fieldID].toReal(), 'f', DBL_DIG);

				int i = stringToDisplay.length() - 1;

				while(i >= 0)
				{
					if(stringToDisplay[i] != '0')
					{
						stringToDisplay.remove(i + 1,
							stringToDisplay.length() - i);
						break;
					}

					--i;
				}

				if(stringToDisplay[stringToDisplay.length() - 1] == '.')
				{
					stringToDisplay.remove(stringToDisplay.length() - 1, 1);
				}
			}
			else if(fieldType == CC::FieldType::Logical)
			{
				int logicalValue = m_recordsList[index.row()]
					.valueByFieldID[fieldID].toInt();

				if(logicalValue > 0)
				{
					stringToDisplay = QObject::trUtf8("Yes");
				}
				else if(logicalValue == 0)
				{
					stringToDisplay = QObject::trUtf8("Unknown");
				}
				else
				{
					stringToDisplay = QObject::trUtf8("No");
				}
			}
			else if(fieldType == CC::FieldType::Date)
			{
				stringToDisplay = m_recordsList[index.row()].
					valueByFieldID[fieldID].toDate().toString("dd.MM.yyyy");
			}
			else if(fieldType == CC::FieldType::Time)
			{
				qulonglong milliseconds = m_recordsList[index.row()].
					valueByFieldID[fieldID].toULongLong();

				stringToDisplay = QString::fromStdWString(
					CC::Duration(milliseconds).toWStringCompact());
			}

			return QVariant(stringToDisplay);
		}
		else if(role == Qt::EditRole)
		{
			return m_recordsList[index.row()].valueByFieldID[fieldID];
		}
	}

	return QVariant();
}

//	END OF QVariant CheckListModel::data(const QModelIndex &index, int role)
//		const
//==============================================================================

QVariant CC::CheckListModel::headerData(int section,
	Qt::Orientation orientation, int role) const
{
	if(section >= m_fieldsList.size() + 1 || orientation != Qt::Horizontal)
	{
		return QVariant();
	}

	if(section == 0)
	{
		if(role == Qt::DisplayRole)
		{
			return QVariant(QString::null);
		}
		else if(role == Qt::ToolTipRole)
		{
			return QVariant(QObject::trUtf8("Record check"));
		}
		else if(role == Qt::DecorationRole)
		{
			return QVariant(QIcon(QString(":check.png")));
		}
#if QT_VERSION >= QT_VERSION_CHECK(4,8,0)
		else if(role == Qt::InitialSortOrderRole)
		{
			return QVariant(Qt::DescendingOrder);
		}
#endif
	}
	else
	{
		if(role == Qt::DisplayRole || role == Qt::ToolTipRole)
		{
			return QVariant(QString::fromStdWString(
				m_fieldsList[section - 1].name));
		}
	}

	return QVariant();
}

//	END OF QVariant CC::CheckListModel::headerData(int section,
//		Qt::Orientation orientation, int role) const
//==============================================================================

int CC::CheckListModel::rowCount(const QModelIndex & /*parent*/) const
{
	return m_recordsList.size();
}

//	END OF int CC::CheckListModel::rowCount(const QModelIndex & parent) const
//==============================================================================

int CC::CheckListModel::columnCount(const QModelIndex & /*parent*/) const
{
	return 1 + m_fieldsList.size();
}

//	END OF int CC::CheckListModel::columnCount(const QModelIndex & parent) const
//==============================================================================

bool CC::CheckListModel::setData(const QModelIndex &index,
	const QVariant &value, int role)
{
	if(index.column() >= m_fieldsList.size() + 1 ||
		index.row() >= m_recordsList.size())
	{
		return false;
	}

	if(index.column() == 0 && role == Qt::CheckStateRole)
	{
		m_recordsList[index.row()].checked = value.toBool();
		// Emitting signal to repaint whole row with new background color.
		emit dataChanged(createIndex(0, index.row()),
			createIndex(m_fieldsList.size(), index.row()));
		emit checkedRecordsNumberChanged();
		return true;
	}

	if(role == Qt::EditRole)
	{
		CC::TFieldID fieldID = m_fieldsList[index.column() - 1].id;
		m_recordsList[index.row()].valueByFieldID[fieldID] = value;
		emit dataChanged(createIndex(index.column() - 1, index.row()),
			createIndex(index.column() - 1, index.row()));
		return true;
	}

	return false;
}

//	END OF bool CC::CheckListModel::setData(const QModelIndex &index,
//		const QVariant &value, int role)
//==============================================================================

bool CC::CheckListModel::setHeaderData(int /*section*/,
	Qt::Orientation /*orientation*/, const QVariant & /*value*/, int /*role*/)
{
	return false;
}

//	END OF bool CC::CheckListModel::setHeaderData(int section,
//		Qt::Orientation orientation, const QVariant &value, int role)
//==============================================================================

void CC::CheckListModel::sort(int column, Qt::SortOrder order)
{
	if(m_recordsList.empty() || column >= m_fieldsList.size() + 1)
	{
		return;
	}

	mergeSortRecords(m_recordsList, column, order);

	emit dataChanged(createIndex(0, 0),
		createIndex(m_fieldsList.size() + 1, m_recordsList.size()));
}

//	END OF void CC::CheckListModel::sort(int column, Qt::SortOrder order)
//==============================================================================

void CC::CheckListModel::mergeSortRecords(QList<CC::Record> &recordsList,
	int column, Qt::SortOrder order)
{
	int listSize = recordsList.size();

	if(listSize <= 1)
	{
		return;
	}

	int middle = listSize / 2;

	QList<CC::Record> list1, list2;

	for(int i = 0; i < middle; ++i)
	{
		list1.append(recordsList.takeFirst());
	}

	for(int i = middle; i < listSize; ++i)
	{
		list2.append(recordsList.takeFirst());
	}

	assert(recordsList.empty());

	mergeSortRecords(list1, column, order);
	mergeSortRecords(list2, column, order);

	while(!list1.empty() || !list2.empty())
	{
		if(list1.empty())
		{
			recordsList.append(list2.takeFirst());
		}
		else if(list2.empty())
		{
			recordsList.append(list1.takeFirst());
		}
		else if(column == 0)
		{
			if(order == Qt::AscendingOrder)
			{
				if(list1.first().checked <= list2.first().checked)
				{
					recordsList.append(list1.takeFirst());
				}
				else
				{
					recordsList.append(list2.takeFirst());
				}
			}
			else
			{
				if(list1.first().checked >= list2.first().checked)
				{
					recordsList.append(list1.takeFirst());
				}
				else
				{
					recordsList.append(list2.takeFirst());
				}
			}
		}
		else
		{
			assert(m_fieldsList.size() >= column);

			CC::TFieldID fieldID = m_fieldsList[column - 1].id;
			CC::TFieldType fieldType = m_fieldsList[column - 1].type;

			if(order == Qt::AscendingOrder)
			{
				if(
					(fieldType == CC::FieldType::Text &&
						QString::localeAwareCompare(
							list1.first().valueByFieldID[fieldID].toString(),
							list2.first().valueByFieldID[fieldID].toString()
						) <= 0
					)
					||
					(fieldType == CC::FieldType::Integer &&
						list1.first().valueByFieldID[fieldID].toLongLong() <=
						list2.first().valueByFieldID[fieldID].toLongLong()
					)
					||
					(fieldType == CC::FieldType::Real &&
						list1.first().valueByFieldID[fieldID].toReal() <=
						list2.first().valueByFieldID[fieldID].toReal()
					)
					||
					(fieldType == CC::FieldType::Logical &&
						list1.first().valueByFieldID[fieldID].toInt() <=
						list2.first().valueByFieldID[fieldID].toInt()
					)
					||
					(fieldType == CC::FieldType::Date &&
						list1.first().valueByFieldID[fieldID].toDate() <=
						list2.first().valueByFieldID[fieldID].toDate()
					)
					||
					(fieldType == CC::FieldType::Time &&
						list1.first().valueByFieldID[fieldID].toULongLong() <=
						list2.first().valueByFieldID[fieldID].toULongLong()
					)
				)
				{
					recordsList.append(list1.takeFirst());
				}
				else
				{
					recordsList.append(list2.takeFirst());
				}
			}
			else // Descending order
			{
				if(
					(fieldType == CC::FieldType::Text &&
						QString::localeAwareCompare(
							list1.first().valueByFieldID[fieldID].toString(),
							list2.first().valueByFieldID[fieldID].toString()
						) >= 0
					)
					||
					(fieldType == CC::FieldType::Integer &&
						list1.first().valueByFieldID[fieldID].toLongLong() >=
						list2.first().valueByFieldID[fieldID].toLongLong()
					)
					||
					(fieldType == CC::FieldType::Real &&
						list1.first().valueByFieldID[fieldID].toReal() >=
						list2.first().valueByFieldID[fieldID].toReal()
					)
					||
					(fieldType == CC::FieldType::Logical &&
						list1.first().valueByFieldID[fieldID].toInt() >=
						list2.first().valueByFieldID[fieldID].toInt()
					)
					||
					(fieldType == CC::FieldType::Date &&
						list1.first().valueByFieldID[fieldID].toDate() >=
						list2.first().valueByFieldID[fieldID].toDate()
					)
					||
					(fieldType == CC::FieldType::Time &&
						list1.first().valueByFieldID[fieldID].toULongLong() >=
						list2.first().valueByFieldID[fieldID].toULongLong()
					)
				)
				{
					recordsList.append(list1.takeFirst());
				}
				else
				{
					recordsList.append(list2.takeFirst());
				}
			}
		}
	}

	assert(recordsList.size() == listSize);
}

//	END OF void CC::CheckListModel::mergeSort(QList<CC::Record> &recordsList,
//		int column, Qt::SortOrder order)
//==============================================================================

const QList<CC::Field> & CC::CheckListModel::fieldsList() const
{
	return m_fieldsList;
}

//	END OF const QList<CC::Field> & QList<CC::Field>
//		CC::CheckListModel::fieldsList() const
//==============================================================================

bool CC::CheckListModel::setFieldsList(QList<CC::Field> newFieldsList)
{
	if(newFieldsList.empty())
	{
		return false;
	}

	assert(m_fieldsList.size() > 0);

	emit layoutAboutToBeChanged();

	if(!m_recordsList.empty())
	{
		// Marking all old fields to be deleted. If matching id is found in the
		// new list, the field will be removed from deletion list.
		QList<CC::Field> fieldsToDelete = m_fieldsList;
		QList<CC::Field> fieldsToAdd;

		bool currentNewFieldExists;

		for(int i = 0; i < newFieldsList.size(); ++i)
		{
			currentNewFieldExists = false;

			QList<CC::Field>::iterator it = fieldsToDelete.begin();
			while(it != fieldsToDelete.end())
			{
				if(newFieldsList[i].id == it->id)
				{
					currentNewFieldExists = true;
					fieldsToDelete.erase(it);
					break;
				}
				++it;
			}

			if(!currentNewFieldExists)
			{
				fieldsToAdd.append(newFieldsList[i]);
			}
		}

		for(int i = 0; i < m_recordsList.size(); ++i)
		{
			for(int j = 0; j < fieldsToDelete.size(); ++j)
			{
				m_recordsList[i].valueByFieldID.remove(fieldsToDelete[j].id);
			}

			for(int j = 0; j < fieldsToAdd.size(); ++j)
			{
				if(fieldsToAdd[j].type == CC::FieldType::Text)
				{
					m_recordsList[i].valueByFieldID.insert(fieldsToAdd[j].id,
						QVariant(QString::null));
				}
				else if(fieldsToAdd[j].type == CC::FieldType::Integer)
				{
					m_recordsList[i].valueByFieldID.insert(fieldsToAdd[j].id,
						QVariant(0));
				}
				else if(fieldsToAdd[j].type == CC::FieldType::Real)
				{
					m_recordsList[i].valueByFieldID.insert(fieldsToAdd[j].id,
						QVariant(0.0));
				}
				else if(fieldsToAdd[j].type == CC::FieldType::Logical)
				{
					m_recordsList[i].valueByFieldID.insert(fieldsToAdd[j].id,
						QVariant(false));
				}
				else if(fieldsToAdd[j].type == CC::FieldType::Date)
				{
					m_recordsList[i].valueByFieldID.insert(fieldsToAdd[j].id,
						QVariant(QDate::currentDate()));
				}
				else if(fieldsToAdd[j].type == CC::FieldType::Time)
				{
					m_recordsList[i].valueByFieldID.insert(fieldsToAdd[j].id,
						QVariant(0ull));
				}
			}
		}
	}

	m_fieldsList = newFieldsList;

	emit layoutChanged();
	return true;
}

//	END OF bool CC::CheckListModel::setFieldsList(QList<CCField> newFieldsList)
//==============================================================================

bool CC::CheckListModel::addRecord()
{
	int newRowNumber = m_recordsList.size();
	beginInsertRows(QModelIndex(), newRowNumber, newRowNumber);

	CC::Record newRecord;

	for(int i = 0; i < m_fieldsList.size(); ++i)
	{
		if(m_fieldsList[i].type == CC::FieldType::Text)
		{
			newRecord.valueByFieldID[m_fieldsList[i].id] =
				QVariant(QString(""));
		}
		else if(m_fieldsList[i].type == CC::FieldType::Integer)
		{
			newRecord.valueByFieldID[m_fieldsList[i].id] =
				QVariant(0ll);
		}
		else if(m_fieldsList[i].type == CC::FieldType::Real)
		{
			newRecord.valueByFieldID[m_fieldsList[i].id] =
				QVariant(0.0);
		}
		else if(m_fieldsList[i].type == CC::FieldType::Logical)
		{
			newRecord.valueByFieldID[m_fieldsList[i].id] =
				QVariant(0);
		}
		else if(m_fieldsList[i].type == CC::FieldType::Date)
		{
			newRecord.valueByFieldID[m_fieldsList[i].id] =
				QVariant(QDate::currentDate());
		}
		else if(m_fieldsList[i].type == CC::FieldType::Time)
		{
			newRecord.valueByFieldID[m_fieldsList[i].id] =
				QVariant(0ull);
		}
	}

	m_recordsList.append(newRecord);

	endInsertRows();

	emit recordsNumberChanged();

	return true;
}

//	END OF bool CC::CheckListModel::addRecord()
//==============================================================================

bool CC::CheckListModel::deleteRecords(QList<int> recordNumbersList)
{
	emit layoutAboutToBeChanged();

	for(int i = m_recordsList.size() - 1; i >= 0 ; --i)
	{
		for(int j = 0; j < recordNumbersList.size(); ++j)
		{
			if(i == recordNumbersList[j])
			{
				m_recordsList.removeAt(i);
				recordNumbersList.removeAt(j);
				break;
			}
		}
	}

	emit layoutChanged();

	emit recordsNumberChanged();

	return true;
}

//	END OF bool CC::CheckListModel::deleteRecords(QList<int> recordNumbersList)
//==============================================================================

bool CC::CheckListModel::saveToFile(const QString & filePath)
{
	pugi::xml_document documentToSave;

	pugi::xml_node fieldsNode = documentToSave.append_child(L"fields");

	for(int i = 0; i < m_fieldsList.size(); ++i)
	{
		pugi::xml_node fieldNode = fieldsNode.append_child(L"field");

		pugi::xml_attribute fieldIdAttribute =
			fieldNode.append_attribute(L"id");
		fieldIdAttribute.set_value(std::to_wstring(m_fieldsList[i].id).c_str());

		pugi::xml_attribute fieldNameAttribute =
			fieldNode.append_attribute(L"name");
		fieldNameAttribute.set_value(m_fieldsList[i].name.c_str());

		pugi::xml_attribute fieldTypeAttribute =
			fieldNode.append_attribute(L"type");

		if(m_fieldsList[i].type == CC::FieldType::Text)
		{
			fieldTypeAttribute.set_value(L"text");
		}
		else if(m_fieldsList[i].type == CC::FieldType::Integer)
		{
			fieldTypeAttribute.set_value(L"integer");
		}
		else if(m_fieldsList[i].type == CC::FieldType::Real)
		{
			fieldTypeAttribute.set_value(L"real");
		}
		else if(m_fieldsList[i].type == CC::FieldType::Logical)
		{
			fieldTypeAttribute.set_value(L"logical");
		}
		else if(m_fieldsList[i].type == CC::FieldType::Date)
		{
			fieldTypeAttribute.set_value(L"date");
		}
		else if(m_fieldsList[i].type == CC::FieldType::Time)
		{
			fieldTypeAttribute.set_value(L"time");
		}
	}

	pugi::xml_node recordsNode = documentToSave.append_child(L"records");

	for(int i = 0; i < m_recordsList.size(); ++i)
	{
		pugi::xml_node recordNode = recordsNode.append_child(L"record");

		pugi::xml_attribute checkedAttribute =
			recordNode.append_attribute(L"checked");
		checkedAttribute.set_value(m_recordsList[i].checked);

		for(int j = 0; j < m_fieldsList.size(); ++j)
		{
			pugi::xml_node valueNode = recordNode.append_child(L"value");

			pugi::xml_attribute fieldIdAttribute =
				valueNode.append_attribute(L"field_id");
			fieldIdAttribute.set_value(
				std::to_wstring(m_fieldsList[j].id).c_str());

			pugi::xml_attribute valueAttribute =
				valueNode.append_attribute(L"value");

			QVariant valueVariant =
				m_recordsList[i].valueByFieldID[m_fieldsList[j].id];

			QString valueString = valueVariant.toString();

			valueAttribute.set_value(valueString.toStdWString().c_str());
		}
	}

	bool saveResult = documentToSave.save_file(filePath.toStdWString().c_str(),
		L"\t", pugi::format_default | pugi::format_write_bom);

	if(saveResult) return true;

	return false;
}

//	END OF bool CC::CheckListModel::saveToFile(const QString & filePath)
//==============================================================================

bool CC::CheckListModel::loadFromFile(const QString & filePath)
{
	pugi::xml_document documentToLoad;

	pugi::xml_parse_result loadResult =
		documentToLoad.load_file(filePath.toStdWString().c_str());
	assert(loadResult);
	if(!loadResult) return false;

	pugi::xml_node fieldsNode = documentToLoad.child(L"fields");
	assert(fieldsNode);
	if(!fieldsNode) return false;

	QList<CC::Field> tempFieldsList;

	QSet<CC::TFieldID> idSet;

	for(pugi::xml_node fieldNode : fieldsNode.children(L"field"))
	{
		CC::Field newField;

		pugi::xml_attribute idAttribute = fieldNode.attribute(L"id");
		assert(idAttribute);
		if(!idAttribute) continue;
		const wchar_t * idValue = idAttribute.value();
		newField.id = QString::fromStdWString(idValue).toULongLong();
		if(idSet.contains(newField.id)) continue;

		pugi::xml_attribute nameAttribute = fieldNode.attribute(L"name");
		assert(nameAttribute);
		if(!nameAttribute) continue;
		newField.name = nameAttribute.value();

		pugi::xml_attribute typeAttribute = fieldNode.attribute(L"type");
		assert(typeAttribute);
		if(!typeAttribute) continue;
		const wchar_t * typeValue = typeAttribute.value();
		QString typeString = QString::fromStdWString(typeValue);
		if(typeString == "text")
		{
			newField.type = CC::FieldType::Text;
		}
		else if(typeString == "integer")
		{
			newField.type = CC::FieldType::Integer;
		}
		else if(typeString == "real")
		{
			newField.type = CC::FieldType::Real;
		}
		else if(typeString == "logical")
		{
			newField.type = CC::FieldType::Logical;
		}
		else if(typeString == "date")
		{
			newField.type = CC::FieldType::Date;
		}
		else if(typeString == "time")
		{
			newField.type = CC::FieldType::Time;
		}

		tempFieldsList.append(newField);
		idSet.insert(newField.id);
	}

	assert(!tempFieldsList.empty());
	if(tempFieldsList.empty()) return false;

	pugi::xml_node recordsNode = documentToLoad.child(L"records");
	assert(recordsNode);
	if(!recordsNode) return false;

	QList<CC::Record> tempRecordsList;

	for(pugi::xml_node recordNode : recordsNode.children(L"record"))
	{
		CC::Record newRecord;

		pugi::xml_attribute checkedAttribute = recordNode.attribute(L"checked");
		assert(checkedAttribute);
		if(!checkedAttribute) continue;
		newRecord.checked = checkedAttribute.as_bool();

		QSet<CC::TFieldID> idSetForRecord;

		for(pugi::xml_node valueNode : recordNode.children(L"value"))
		{
			pugi::xml_attribute idAttribute = valueNode.attribute(L"field_id");
			assert(idAttribute);
			if(!idAttribute) continue;
			CC::TFieldID fieldId =
				QString::fromStdWString(idAttribute.value()).toULongLong();
			if(!idSet.contains(fieldId) || idSetForRecord.contains(fieldId))
				continue;
			idSetForRecord.insert(fieldId);

			pugi::xml_attribute valueAttribute = valueNode.attribute(L"value");
			assert(valueAttribute);
			if(!valueAttribute) continue;

			newRecord.valueByFieldID[fieldId] =
				QVariant(QString::fromStdWString(valueAttribute.value()));
		}

		assert(!newRecord.valueByFieldID.empty());
		if(newRecord.valueByFieldID.empty()) continue;

		tempRecordsList.append(newRecord);
	}

	assert(!tempRecordsList.empty());
	if(tempRecordsList.empty()) return false;

	emit layoutAboutToBeChanged();

	m_fieldsList.swap(tempFieldsList);
	m_recordsList.swap(tempRecordsList);

	emit layoutChanged();

	emit recordsNumberChanged();

	return true;
}

//	END OF bool CC::CheckListModel::loadFromFile(const QString & filePath)
//==============================================================================

int CC::CheckListModel::recordsNumber()
{
	return m_recordsList.size();
}

//	END OF int CC::CheckListModel::recordsNumber()
//==============================================================================

int CC::CheckListModel::checkedRecordsNumber()
{
	int checkedRecordsFound = 0;

	for(int i = 0; i < m_recordsList.size(); ++i)
	{
		if(m_recordsList[i].checked)
		{
			++checkedRecordsFound;
		}
	}

	return checkedRecordsFound;
}

//	END OF int CC::CheckListModel::checkedRecordsNumber()
//==============================================================================


