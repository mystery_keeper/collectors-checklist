#ifndef ITEMDELEGATEFORINTEGER_H_INCLUDED
#define ITEMDELEGATEFORINTEGER_H_INCLUDED

#include <QStyledItemDelegate>

namespace CC
{
	/// ItemView delegate for integer type item.
	class ItemDelegateForInteger final : public QStyledItemDelegate
	{
		Q_OBJECT

		public:

			/// Constructor.
			ItemDelegateForInteger(QObject * parent = nullptr);

			/// Destructor.
			virtual ~ItemDelegateForInteger();

			/// Creates editor widget for the item.
			/// Reimplemented from QStyledItemDelegate.
			virtual QWidget * createEditor(QWidget * parent,
				const QStyleOptionViewItem & option, const QModelIndex & index)
				const override;

			/// Sets data for the editor widget.
			/// Reimplemented from QStyledItemDelegate.
			virtual void setEditorData(QWidget * editor,
				const QModelIndex & index) const override;

			/// Adjusts editor size and position for the view item.
			/// Reimplemented from QStyledItemDelegate.
			virtual void updateEditorGeometry(QWidget * editor,
				const QStyleOptionViewItem & option, const QModelIndex & index)
				const override;

			/// Sets model item data from the editor.
			/// Reimplemented from QStyledItemDelegate.
			virtual void setModelData(QWidget * editor,
				QAbstractItemModel * model, const QModelIndex & index) const
				override;
	};
}

#endif // ITEMDELEGATEFORINTEGER_H_INCLUDED
