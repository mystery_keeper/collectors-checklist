#include <QDateEdit>

#include "itemdelegatefordate.h"

//==============================================================================

CC::ItemDelegateForDate::ItemDelegateForDate(QObject * parent):
	QStyledItemDelegate(parent)
{

}

//	END OF CC::ItemDelegateForDate::ItemDelegateForDate(
//		QObject * parent)
//==============================================================================

CC::ItemDelegateForDate::~ItemDelegateForDate()
{

}

//	END OF CC::ItemDelegateForDate::~ItemDelegateForDate()
//==============================================================================

QWidget * CC::ItemDelegateForDate::createEditor(QWidget * parent,
	const QStyleOptionViewItem & /*option*/, const QModelIndex & /*index*/)
	const
{
	QDateEdit * dateEdit = new QDateEdit(parent);

//	dateEdit->setCalendarPopup(true);

	return dateEdit;
}

//	END OF QWidget * CC::ItemDelegateForDate::createEditor(
//		QWidget * parent, const QStyleOptionViewItem & option,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForDate::setEditorData(QWidget * editor,
	const QModelIndex & index) const
{
	QDateEdit * dateEdit = (QDateEdit *)editor;

	QVariant newValue = index.model()->data(index, Qt::EditRole);

	dateEdit->setDate(newValue.toDate());

	return;
}

//	END OF CC::ItemDelegateForDate::setEditorData(QWidget * editor,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForDate::updateEditorGeometry(QWidget * editor,
	const QStyleOptionViewItem & option, const QModelIndex & /*index*/)
	const
{
	editor->setGeometry(option.rect);
	return;
}

//	END OF void CC::ItemDelegateForDate::updateEditorGeometry(
//		QWidget * editor, const QStyleOptionViewItem & option,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForDate::setModelData(QWidget * editor,
	QAbstractItemModel * model, const QModelIndex & index) const
{
	QDateEdit * dateEdit = (QDateEdit *)editor;
	QVariant newValue(dateEdit->date());
	model->setData(index, newValue, Qt::EditRole);
	return;
}

//	END OF void CC::ItemDelegateForDate::setModelData(QWidget * editor,
//		QAbstractItemModel * model, const QModelIndex & index) const
//==============================================================================
