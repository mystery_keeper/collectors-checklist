#include <QLineEdit>

#include "itemdelegatefortext.h"

//==============================================================================

CC::ItemDelegateForText::ItemDelegateForText(QObject * parent):
	QStyledItemDelegate(parent)
{

}

//	END OF CC::ItemDelegateForText::ItemDelegateForText(
//		QObject * parent)
//==============================================================================

CC::ItemDelegateForText::~ItemDelegateForText()
{

}

//	END OF CC::ItemDelegateForText::~ItemDelegateForText()
//==============================================================================

QWidget * CC::ItemDelegateForText::createEditor(QWidget * parent,
	const QStyleOptionViewItem & /*option*/, const QModelIndex & /*index*/)
	const
{
	QLineEdit * lineEdit = new QLineEdit(parent);

	return lineEdit;
}

//	END OF QWidget * CC::ItemDelegateForText::createEditor(
//		QWidget * parent, const QStyleOptionViewItem & option,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForText::setEditorData(QWidget * editor,
	const QModelIndex & index) const
{
	QLineEdit * lineEdit = (QLineEdit *)editor;

	QVariant newValue = index.model()->data(index, Qt::EditRole);

	lineEdit->setText(newValue.toString());

	return;
}

//	END OF CC::ItemDelegateForText::setEditorData(QWidget * editor,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForText::updateEditorGeometry(QWidget * editor,
	const QStyleOptionViewItem & option, const QModelIndex & /*index*/)
	const
{
	editor->setGeometry(option.rect);
	return;
}

//	END OF void CC::ItemDelegateForText::updateEditorGeometry(
//		QWidget * editor, const QStyleOptionViewItem & option,
//		const QModelIndex & index) const
//==============================================================================

void CC::ItemDelegateForText::setModelData(QWidget * editor,
	QAbstractItemModel * model, const QModelIndex & index) const
{
	QLineEdit * lineEdit = (QLineEdit *)editor;
	QVariant newValue(lineEdit->text());
	model->setData(index, newValue, Qt::EditRole);
	return;
}

//	END OF void CC::ItemDelegateForText::setModelData(QWidget * editor,
//		QAbstractItemModel * model, const QModelIndex & index) const
//==============================================================================
