﻿#include <QList>
#include <QMessageBox>
#include <QFileDialog>
#include <QKeySequence>
#include <QStandardPaths>
#include <QCoreApplication>
#include <assert.h>

#include "mainwindow.h"
#include "ccfield.h"

//==============================================================================

MainWindow::MainWindow(QWidget * parent, Qt::WindowFlags flags):
	QMainWindow(parent, flags)
	, m_pActionNewChecklist(nullptr)
	, m_pActionOpenChecklist(nullptr)
	, m_pActionSaveChecklist(nullptr)
	, m_pActionSaveChecklistAs(nullptr)
	, m_pActionAddChecklistRecord(nullptr)
	, m_pActionDeleteSelectedRecords(nullptr)
	, m_pActionAdjustChecklistFields(nullptr)
	, m_pActionExit(nullptr)
	, m_pActionAbout(nullptr)
	, m_pCurrentCheckListFile(nullptr)
	, m_pItemDelegateForText(nullptr)
	, m_pItemDelegateForInteger(nullptr)
	, m_pItemDelegateForReal(nullptr)
	, m_pItemDelegateForLogical(nullptr)
	, m_pItemDelegateForDate(nullptr)
	, m_pItemDelegateForDuration(nullptr)
	, m_pFieldsListEditDialog(nullptr)
	, m_pRecordsNumberLabel(nullptr)
	, m_pSpacerWidget(nullptr)
{
	ui.setupUi(this);
	ui.menuBar->setContextMenuPolicy(Qt::NoContextMenu);
	ui.toolBar->setMovable(false);
	ui.toolBar->setIconSize(QSize(16, 16));

	createActions();

	m_pItemDelegateForText = new CC::ItemDelegateForText(this);
	m_pItemDelegateForInteger = new CC::ItemDelegateForInteger(this);
	m_pItemDelegateForReal = new CC::ItemDelegateForReal(this);
	m_pItemDelegateForLogical = new CC::ItemDelegateForLogical(this);
	m_pItemDelegateForDate = new CC::ItemDelegateForDate(this);
	m_pItemDelegateForDuration = new CC::ItemDelegateForDuration(this);

	m_pFieldsListEditDialog = new CC::FieldsListEditDialog(this,
		Qt::Dialog
		| Qt::CustomizeWindowHint
		| Qt::WindowTitleHint
		);

	m_pRecordsNumberLabel = new QLabel;
	m_pRecordsNumberLabel->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);

	ui.statusBar->insertPermanentWidget(-1, m_pRecordsNumberLabel, 0);

	m_pSpacerWidget = new QWidget;

	ui.statusBar->insertPermanentWidget(-1, m_pSpacerWidget, 1);

	recreateCurrentChecklist();

	QStringList applicationArguments = QCoreApplication::arguments();

	if(applicationArguments.size() > 1)
	{
		bool loadResult = m_pCurrentCheckListFile->loadFromFileAndRename(
			applicationArguments.last());

		if(loadResult)
		{
			resetViewItemDelegates();
			ui.checkListView->resizeColumnsToContents();
		}
	}

	ui.checkListView->horizontalHeader()->resizeSection(0, 24);
	ui.checkListView->horizontalHeader()->setSectionResizeMode(0,
		QHeaderView::Fixed);
}

//	END OF MainWindow::MainWindow()
//==============================================================================

MainWindow::~MainWindow()
{

}

//	END OF MainWindow::~MainWindow()
//==============================================================================

void MainWindow::closeEvent(QCloseEvent * event)
{
	if(!promptToSaveAndConfirmDiscardFile()) event->ignore();
}

//	END OF void MainWindow::closeEvent(QCloseEvent * event)
//==============================================================================

void MainWindow::createActions()
{
	QMenu * pFileMenu = ui.menuBar->addMenu(QObject::trUtf8("File"));

	m_pActionNewChecklist = new QAction(this);
	m_pActionNewChecklist->setText(QObject::trUtf8("New checklist"));
	m_pActionNewChecklist->setIcon(
		QIcon(QString(":newchecklist.png")));
	pFileMenu->addAction(m_pActionNewChecklist);
	ui.toolBar->addAction(m_pActionNewChecklist);

	/// \cond
	connect(m_pActionNewChecklist, SIGNAL(triggered()),
		this, SLOT(slotNewChecklist()));
	/// \endcond

//------------------------------------------------------------------------------

	m_pActionOpenChecklist = new QAction(this);
	m_pActionOpenChecklist->setText(QObject::trUtf8("Open checklist"));
	m_pActionOpenChecklist->setIcon(
		QIcon(QString(":loadchecklist.png")));
	pFileMenu->addAction(m_pActionOpenChecklist);
	ui.toolBar->addAction(m_pActionOpenChecklist);

	/// \cond
	connect(m_pActionOpenChecklist, SIGNAL(triggered()),
		this, SLOT(slotOpenChecklist()));
	/// \endcond

//------------------------------------------------------------------------------

	m_pActionSaveChecklist = new QAction(this);
	m_pActionSaveChecklist->setText(QObject::trUtf8("Save checklist"));
	m_pActionSaveChecklist->setIcon(
		QIcon(QString(":savechecklist.png")));
	pFileMenu->addAction(m_pActionSaveChecklist);
	ui.toolBar->addAction(m_pActionSaveChecklist);

	/// \cond
	connect(m_pActionSaveChecklist, SIGNAL(triggered()),
		this, SLOT(slotSaveChecklist()));
	/// \endcond

//------------------------------------------------------------------------------

	m_pActionSaveChecklistAs = new QAction(this);
	m_pActionSaveChecklistAs->setText(QObject::trUtf8("Save checklist as..."));
	m_pActionSaveChecklistAs->setIcon(
		QIcon(QString(":savechecklistas.png")));
	pFileMenu->addAction(m_pActionSaveChecklistAs);
	ui.toolBar->addAction(m_pActionSaveChecklistAs);

	/// \cond
	connect(m_pActionSaveChecklistAs, SIGNAL(triggered()),
		this, SLOT(slotSaveChecklistAs()));
	/// \endcond

//------------------------------------------------------------------------------

	pFileMenu->addSeparator();

//------------------------------------------------------------------------------

	m_pActionExit = new QAction(this);
	m_pActionExit->setText(QObject::trUtf8("Exit"));
	m_pActionExit->setIcon(
		QIcon(QString(":exit.png")));
	pFileMenu->addAction(m_pActionExit);

	/// \cond
	connect(m_pActionExit, SIGNAL(triggered()), this, SLOT(close()));
	/// \endcond

//------------------------------------------------------------------------------

	QMenu * pEditMenu = ui.menuBar->addMenu(QObject::trUtf8("Edit"));

	ui.toolBar->addSeparator();

//------------------------------------------------------------------------------

	m_pActionAdjustChecklistFields = new QAction(this);
	m_pActionAdjustChecklistFields->setText(
		QObject::trUtf8("Adjust checklist fields"));
	m_pActionAdjustChecklistFields->setIcon(
		QIcon(QString(":setfields.png")));
	pEditMenu->addAction(m_pActionAdjustChecklistFields);
	ui.toolBar->addAction(m_pActionAdjustChecklistFields);

	/// \cond
	connect(m_pActionAdjustChecklistFields, SIGNAL(triggered()),
		this, SLOT(slotAdjustChecklistFields()));
	/// \endcond

//------------------------------------------------------------------------------

	m_pActionAddChecklistRecord = new QAction(this);
	m_pActionAddChecklistRecord->setText(
		QObject::trUtf8("Add new record"));
	m_pActionAddChecklistRecord->setIcon(
		QIcon(QString(":addrow.png")));
	pEditMenu->addAction(m_pActionAddChecklistRecord);
	ui.toolBar->addAction(m_pActionAddChecklistRecord);

	/// \cond
	connect(m_pActionAddChecklistRecord, SIGNAL(triggered()),
		this, SLOT(slotAddChecklistRecord()));
	/// \endcond

//------------------------------------------------------------------------------

	m_pActionDeleteSelectedRecords = new QAction(this);
	m_pActionDeleteSelectedRecords->setText(
		QObject::trUtf8("Delete selected records"));
	m_pActionDeleteSelectedRecords->setIcon(
		QIcon(QString(":deleterows.png")));
	m_pActionDeleteSelectedRecords->setShortcut(QKeySequence(Qt::Key_Delete));
	pEditMenu->addAction(m_pActionDeleteSelectedRecords);
	ui.toolBar->addAction(m_pActionDeleteSelectedRecords);

	/// \cond
	connect(m_pActionDeleteSelectedRecords, SIGNAL(triggered()),
		this, SLOT(slotDeleteSelectedRecords()));
	/// \endcond

//------------------------------------------------------------------------------

	QMenu * pHelpMenu = ui.menuBar->addMenu(QObject::trUtf8("Help"));

	m_pActionAbout = new QAction(this);
	m_pActionAbout->setText(QObject::trUtf8("About..."));
	pHelpMenu->addAction(m_pActionAbout);

	/// \cond
	connect(m_pActionAbout, SIGNAL(triggered()),
		this, SLOT(slotAbout()));
	/// \endcond
}

//	END OF void MainWindow::createActions()
//==============================================================================

void MainWindow::resetViewItemDelegates()
{
	if(m_pCurrentCheckListFile == nullptr) return;

	CC::CheckListModel * pCurrentModel = m_pCurrentCheckListFile->model();

	assert(pCurrentModel != nullptr);
	assert(m_pItemDelegateForText != nullptr);
	assert(m_pItemDelegateForInteger != nullptr);
	assert(m_pItemDelegateForReal != nullptr);
	assert(m_pItemDelegateForLogical != nullptr);
	assert(m_pItemDelegateForDate != nullptr);
	assert(m_pItemDelegateForDuration != nullptr);

	const QList<CC::Field> & fieldsList = pCurrentModel->fieldsList();

	for(int i = 0; i < fieldsList.size(); ++i)
	{
		if(fieldsList[i].type == CC::FieldType::Text)
		{
			ui.checkListView->setItemDelegateForColumn(i + 1,
				m_pItemDelegateForText);
		}
		else if(fieldsList[i].type == CC::FieldType::Integer)
		{
			ui.checkListView->setItemDelegateForColumn(i + 1,
				m_pItemDelegateForInteger);
		}
		else if(fieldsList[i].type == CC::FieldType::Real)
		{
			ui.checkListView->setItemDelegateForColumn(i + 1,
				m_pItemDelegateForReal);
		}
		else if(fieldsList[i].type == CC::FieldType::Logical)
		{
			ui.checkListView->setItemDelegateForColumn(i + 1,
				m_pItemDelegateForLogical);
		}
		else if(fieldsList[i].type == CC::FieldType::Date)
		{
			ui.checkListView->setItemDelegateForColumn(i + 1,
				m_pItemDelegateForDate);
		}
		else if(fieldsList[i].type == CC::FieldType::Time)
		{
			ui.checkListView->setItemDelegateForColumn(i + 1,
				m_pItemDelegateForDuration);
		}
	}
}

//	END OF void MainWindow::createActions()
//==============================================================================

bool MainWindow::promptToSaveAndConfirmDiscardFile()
{
	assert(m_pCurrentCheckListFile != nullptr);

	if((m_pCurrentCheckListFile->state() == CC::CheckListFile::State::Unchanged)
		|| (m_pCurrentCheckListFile->state() == CC::CheckListFile::State::New))
	{
		return true;
	}

	QMessageBox::StandardButton answer = QMessageBox::question(this,
		QObject::trUtf8("Save changes to file?"),
		QObject::trUtf8("You're about to discard changed file.\n"
		"Would you like to save it?"),
		QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);

	if(answer == QMessageBox::Cancel)
	{
		return false;
	}
	else if(answer == QMessageBox::No)
	{
		return true;
	}
	else
	{
		bool fileSavedSuccessfully = false;

		if(m_pCurrentCheckListFile->state() ==
			CC::CheckListFile::State::Changed)
		{
			fileSavedSuccessfully = m_pCurrentCheckListFile->save();
		}
		else // State == New
		{
			QString defaultFilePath = QStandardPaths::writableLocation(
				QStandardPaths::StandardLocation::DocumentsLocation);

			if(defaultFilePath.isEmpty())
			{
				defaultFilePath = QCoreApplication::applicationDirPath();
			}

			defaultFilePath += "/New checklist.checklist";

			QString filePath = QFileDialog::getSaveFileName(this,
				trUtf8("Save checklist as..."), defaultFilePath);

			if(filePath.isEmpty()) return false;

			fileSavedSuccessfully =
				m_pCurrentCheckListFile->saveToFileAndRename(filePath);
		}

		if(!fileSavedSuccessfully)
		{
			answer = QMessageBox::question(this,
			QObject::trUtf8("Save failed."),
			QObject::trUtf8("Failed to save changes to file.\n"
			"Discard all changes and continue current operation?"),
			QMessageBox::Yes | QMessageBox::Cancel);

			if(answer == QMessageBox::Yes) return true;
			else return false;
		}
	}

	return false;
}

//	END OF bool MainWindow::promptToSaveAndConfirmDiscardFile()
//==============================================================================

void MainWindow::recreateCurrentChecklist()
{
	if(m_pCurrentCheckListFile != nullptr)
	{
		delete m_pCurrentCheckListFile;
	}

	CC::Field newField;
	newField.id = 0;
	newField.type = CC::FieldType::Text;
	newField.name = L"Name";

	QList<CC::Field> fieldsList;
	fieldsList.append(newField);

	m_pCurrentCheckListFile = new CC::CheckListFile(fieldsList,
		QString(""), this);

	/// \cond
	connect(m_pCurrentCheckListFile, SIGNAL(signalStateOrPathChanged(
		CC::CheckListFile::State, const QString &)),
		this, SLOT(slotFileStateOrPathChanged(CC::CheckListFile::State,
		const QString &)));
	/// \endcond

	QString newTitle("Collector's Checklist - ");
	newTitle += m_pCurrentCheckListFile->filePath();
	newTitle += " *";
	setWindowTitle(newTitle);

	CC::CheckListModel * pCurrentModel = m_pCurrentCheckListFile->model();
	assert(pCurrentModel != nullptr);

	/// \cond
	connect(pCurrentModel, SIGNAL(recordsNumberChanged()),
		this, SLOT(slotRefreshRecordsNumberLabel()));
	connect(pCurrentModel, SIGNAL(checkedRecordsNumberChanged()),
		this, SLOT(slotRefreshRecordsNumberLabel()));
	/// \endcond

	assert(ui.checkListView != nullptr);
	ui.checkListView->setModel(pCurrentModel);

	resetViewItemDelegates();
	slotRefreshRecordsNumberLabel();
}

//	END OF void MainWindow::recreateCurrentChecklist()
//==============================================================================

void MainWindow::slotAdjustChecklistFields()
{
	if(m_pCurrentCheckListFile == nullptr) return;

	CC::CheckListModel * pCurrentModel = m_pCurrentCheckListFile->model();
	assert(pCurrentModel != nullptr);

	int result = m_pFieldsListEditDialog->callToEditFieldsList(
		pCurrentModel->fieldsList());

	if(result == QDialog::Accepted)
	{
		const QList<CC::Field> & newFieldsList =
			m_pFieldsListEditDialog->fieldsList();

		bool setFieldsListSuccess = pCurrentModel->setFieldsList(
			newFieldsList);

		if(setFieldsListSuccess)
		{
			ui.checkListView->resizeColumnsToContents();
			ui.checkListView->horizontalHeader()->resizeSection(0, 24);

			resetViewItemDelegates();
		}
	}
}

//	END OF void MainWindow::slotAdjustChecklistFields()
//==============================================================================

void MainWindow::slotAddChecklistRecord()
{
	if(m_pCurrentCheckListFile == nullptr) return;

	CC::CheckListModel * pCurrentModel = m_pCurrentCheckListFile->model();
	assert(pCurrentModel != nullptr);

	pCurrentModel->addRecord();

	slotRefreshRecordsNumberLabel();
}

//	END OF void MainWindow::slotAddChecklistRecord()
//==============================================================================

void MainWindow::slotDeleteSelectedRecords()
{
	if(m_pCurrentCheckListFile == nullptr) return;

	CC::CheckListModel * pCurrentModel = m_pCurrentCheckListFile->model();
	assert(pCurrentModel != nullptr);

	QModelIndexList selectedRecordsIndexList =
		ui.checkListView->selectionModel()->selectedRows();

	if(selectedRecordsIndexList.empty())
	{
		return;
	}

	QMessageBox::StandardButton answer = QMessageBox::question(this,
		QObject::trUtf8("Records deletion"),
		QObject::trUtf8("Records selected: ") +
		QString::number(selectedRecordsIndexList.size()) + QString("\n") +
		QObject::trUtf8("Are you sure you want to delete these records?"),
		QMessageBox::Yes | QMessageBox::No);

	if(answer == QMessageBox::No)
	{
		return;
	}

	//else

	QList<int> selectedRecordsNumbersList;

	for(int i = 0; i < selectedRecordsIndexList.size(); ++i)
	{
		selectedRecordsNumbersList.append(selectedRecordsIndexList[i].row());
	}

	assert(!selectedRecordsNumbersList.empty());

	pCurrentModel->deleteRecords(selectedRecordsNumbersList);

	ui.checkListView->clearSelection();

	slotRefreshRecordsNumberLabel();
}

//	END OF void MainWindow::slotDeleteSelectedRecords()
//==============================================================================

void MainWindow::slotNewChecklist()
{
	if((m_pCurrentCheckListFile != nullptr) &&
		(promptToSaveAndConfirmDiscardFile() == false))
	{
		return;
	}

	recreateCurrentChecklist();
}

//	END OF void MainWindow::slotNewChecklist()
//==============================================================================

void MainWindow::slotSaveChecklist()
{
	if(m_pCurrentCheckListFile == nullptr) return;

	if(m_pCurrentCheckListFile->state() == CC::CheckListFile::State::Changed)
	{
		m_pCurrentCheckListFile->save();
	}
	else if(m_pCurrentCheckListFile->state() == CC::CheckListFile::State::New
		|| m_pCurrentCheckListFile->state() ==
		CC::CheckListFile::State::NewAndChanged)
	{
		slotSaveChecklistAs();
	}
}

//	END OF void MainWindow::slotSaveChecklist()
//==============================================================================

void MainWindow::slotSaveChecklistAs()
{
	if(m_pCurrentCheckListFile == nullptr) return;

	QString filePath = QFileDialog::getSaveFileName(this,
		trUtf8("Save checklist as..."), m_pCurrentCheckListFile->filePath());

	if(filePath.isEmpty()) return;

	m_pCurrentCheckListFile->saveToFileAndRename(filePath);
}

//	END OF void MainWindow::slotSaveChecklistAs()
//==============================================================================

void MainWindow::slotOpenChecklist()
{
	if(m_pCurrentCheckListFile == nullptr) return;

	if(!promptToSaveAndConfirmDiscardFile()) return;

	QString filePath = QFileDialog::getOpenFileName(this,
		trUtf8("Open cheklist"),
		QDir::toNativeSeparators(m_pCurrentCheckListFile->filePath()),
		trUtf8("Checklists (*.checklist);;All files (*)"));

	if(filePath.isEmpty()) return;

	bool loadedSuccessfuly =
		m_pCurrentCheckListFile->loadFromFileAndRename(filePath);

	if(loadedSuccessfuly)
	{
		ui.checkListView->resizeColumnsToContents();
		ui.checkListView->horizontalHeader()->resizeSection(0, 24);
		resetViewItemDelegates();
	}
}

//	END OF vvoid MainWindow::slotOpenChecklist()
//==============================================================================

void MainWindow::slotRefreshRecordsNumberLabel()
{
	if(m_pCurrentCheckListFile == nullptr) return;

	CC::CheckListModel * pCurrentModel = m_pCurrentCheckListFile->model();
	assert(pCurrentModel != nullptr);

	if(m_pRecordsNumberLabel == nullptr) return;

	QString labelText = QString("%1/%2")
		.arg(pCurrentModel->checkedRecordsNumber())
		.arg(pCurrentModel->recordsNumber());

	m_pRecordsNumberLabel->setText(labelText);
}

//	END OF void MainWindow::slotRefreshRecordsNumberLabel()
//==============================================================================

void MainWindow::slotFileStateOrPathChanged(CC::CheckListFile::State newState,
	const QString & newFilePath)
{
	QString newTitle("Collector's Checklist - ");
	newTitle += newFilePath;

	if(newState != CC::CheckListFile::State::Unchanged)
	{
		newTitle += " *";
	}

	setWindowTitle(newTitle);
}

//	END OF void MainWindow::slotFileStateOrPathChanged(
//		CC::CheckListFile::State newState, const QString & newFilePath)
//==============================================================================

void MainWindow::slotAbout()
{
	QMessageBox::about(this, trUtf8("About Collector's Checklist"),trUtf8(
		"Collector's Checklist r1\n"
		"\n"
		"By Aleksey [Mystery Keeper] Lyashin\n"
		"mystkeeper@gmail.com\n"
		"http://mysterykeeper.ru\n"
		"\n"
		"This software is free and distributed under MIT license.\n"
		"\n"
		"Software uses Qt 5 framework by Digia, "
		"distributed under LGPL license.\n"
		"https://qt.digia.com/\n"
		"\n"
		"Software source files include pugixml library by Arseny Kapoulkine,\n"
		"distributed under MIT license.\n"
		"https://code.google.com/p/pugixml/\n"
		"\n"
		"Software includes Silk icons v1.3 by Mark James,\n"
		"distributed under Creative Commons Attribution 2.5 License.\n"
		"http://www.famfamfam.com/lab/icons/silk/\n"
	));
}

//	END OF void MainWindow::slotAbout()
//==============================================================================
